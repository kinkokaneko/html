(function() {
    'use strict';
  
    
//入力文字を返す------------------------------------------------------  
    var vm = new Vue({
      el: '#app',
      data: {
        name: ''
      }
    });

//Componentの使い方--------------------------------------------------
var likeComponent=Vue.extend({

  props:['message'],

  data:function(){
    return{
      count:0
    }
  },

  template:'<button @click="countUp">{{message}} {{count}}</button>',
  methods:{
    countUp:function(){
      this.count++;
      this.$emit('increment')
    }
  }
})

var vm = new Vue({
  el: '#com',
  components: {
    'like-component':likeComponent
  },
  data:{
    total:0
  },
  methods:{
    incrementTotal:function(){
      this.total++;
    }
  }
});

//todoアプリ--------------------------------------------------------
    var vx = new Vue({
        el: '#todo',
        data: {

          newItem:'',
          todos: []
        },

        //リロード時にLocalStrageから配列を読み込み表示する
        mounted:function() {
          this.todos=JSON.parse(localStorage.getItem('todos'))||[];
          
        },

        //watchで指定した処理の監視をする
        //todosに何らかの変化が行われたときにデータを保存する。リロードしても消えない
        watch:{
          todos:{
          handler:function(){
            //localStrageにtodosの値をJSON形式で保存
          localStorage.setItem('todos',JSON.stringify(this.todos));
          //alert('Data saved')
          },
          deep:true
        }},

        methods: {
          
          //タスク追加
          addItem: function() {
            var item = {
              title: this.newItem,
              isDone: false
            };
            this.todos.push(item);
            this.newItem = '';
          },

          //タスク削除
          deleteItem: function(index) {
            if (confirm('are you sure?')) {
              this.todos.splice(index, 1);
            }
          },
          //完了したタスク全削除
          purge: function() {
            if (!confirm('delete finished')) {
              return;
            }
            this.todos=this.remaining;
          
        }
        },

        //タスクの総数と完了数を算出
        computed:{
          remaining:function(){
            // var items=this.todos.filter(function(todo){
            //   return !todo.isDone;
            // });
            // return items.length;
            return this.todos.filter(function(todo){
              return !todo.isDone;
            });
          }

        }

      });
  })();