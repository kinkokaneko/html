#!/bin/bash

#04 特殊変数
# name="kaneko"
# echo "kaneko $1"

#05 ユーザー入力
# read name
# echo "hello $name"
# read -p "名前：" name
# echo "hello $name"

#06 配列
# colors=(red pink red)
# echo ${colors[0]}
# echo ${colors[1]}
# echo ${colors[2]}
# echo ${colors[@]}
# echo ${#colors[@]}

#07　数値計算
# echo `expr 5 + 2`
# echo $((5 + 2))

# 13　ファイルの内容を処理する
# i=1;
# while read line;do
#      echo $i "$line"
#      ((i++))
# done< color.txt


#15 selectでメニューを作成

# select color in red blue yellow green; do
#   case "$color" in
#     red)
#       echo "stop"
#       ;;
#     blue|green)
#       echo "go"
#       ;;
#     yellow)
#       echo "caution"
#       ;;
#     *)
#       echo "wrong signal"
#       break
#   esac
# done

# 関数
hello(){
  # echo "hello..."
  if [[ $1 == "kaneko" ]]; then
    return 0
  else
    return 1
  fi
}

hello kaneko; echo $?
hello kimura; echo $?