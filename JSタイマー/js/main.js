(function () {
    'use strict'

    //要素取得
    var timer = document.getElementById('timer')
    var min = document.getElementById('min')
    var sec = document.getElementById('sec')
    var reset = document.getElementById('reset')
    var start = document.getElementById('start')

    //タイマー計算変数
    var startTime;
    var timeLeft;
    var timeToCountDown = 0;

    var timerId;
    var isRunning = false;//フラグ

    //タイマー表示関数
    function updateTimer(t) {
        var d = new Date(t);
        var m = d.getMinutes();
        var s = d.getSeconds();
        var ms = d.getMilliseconds();
        var timerString;

        //取得した時刻を整形
        m = ('0' + m).slice(-2);
        s = ('0' + s).slice(-2);
        ms = ('00' + ms).slice(-3);

        //タイマー表示
        timerString = m + ':' + s + '.' + ms;
        timer.textContent = timerString;
        
        //タブに残り時間を表示させる
        document.title = timerString;

    }



    function countDown() {
        timerId = setTimeout(function () {
            timeLeft = timeToCountDown - (Date.now() - startTime);
            //console.log(timeLeft);
            //０で止める
            if (timeLeft < 0) {
                isRunning = false;
                start.textContent = 'start';
                clearTimeout(timerId);
                timeLeft = 0;
                timeToCountDown = 0;//終了したあとにスタートを押しても０のままにする
                updateTimer(timeLeft);
                return;
            }
            updateTimer(timeLeft);
            countDown();
        }, 10);
    }

    //ボタンクリック処理
    start.addEventListener('click', function () {

        if (isRunning === false) {
            //スタートのボタンをストップに変更
            isRunning = true;
            start.textContent = 'stop';
            startTime = Date.now();
            countDown();
        } else {
            //ストップボタンをスタートに変更
            isRunning = false;
            start.textContent = 'start';
            timeToCountDown = timeLeft;
            clearTimeout(timerId);
        }


    });

    min.addEventListener('click', function () {
        
        if (isRunning === true) { //カウントダウン中の操作不可の処理
            return;
        }

        timeToCountDown += 60 * 1000;
        updateTimer(timeToCountDown);
    })

    sec.addEventListener('click', function () {
        if (isRunning === true) {
            return;
        }
        timeToCountDown += 1000;
        updateTimer(timeToCountDown);
    })

    reset.addEventListener('click', function () {
        timeToCountDown = 0;
        updateTimer(timeToCountDown);
    })

})();