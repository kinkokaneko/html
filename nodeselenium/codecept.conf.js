exports.config = {
  tests: '/*_test.js',
  output: './output',
  helpers: {
    Puppeteer: {
      url: 'https://kids.yahoo.co.jp/',
      show: true,
    }
  },
  translation: './ja-SR.js',
  include: {},
  bootstrap: null,
  mocha: {},
  name: 'nodeselenium',
  translation: 'ja-JP'
}
