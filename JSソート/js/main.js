(function () {
    'use strict'
    var ths = document.getElementsByTagName('th');
    var i;
    var sortOrder = 1;//1:昇順 -1:降順


    function rebuildtbody(rows) {
        //tbody取得
        var tbody = document.querySelector('tbody');
        //tbodyの最初の子要素がある限りtbodyの子要素を削除する
        while (tbody.firstChild) {
            tbody.removeChild(tbody.firstChild);
        }

        //並び替えられたrowsの要素を追加する

        for (i = 0; i < rows.length; i++) {
            tbody.appendChild(rows[i]);
        }
    }


    function updateClassName(th) {
        //sort時にクラスを付与
        var k;
        for (k = 0; k < ths.length; k++) {
            ths[k].className = '';
        }
        th.className = sortOrder === 1 ? 'asc' : 'desc';
    }


    //sort処理
    function compare(a, b, col, type) {
        var _a = a.children[col].textContent
        var _b = b.children[col].textContent


        if (type === "number") {
            //数値変換
            _a = _a * 1;
            _b = _b * 1;
        }
        if (type === "string") {
            _a = _a.toLowerCase();
            _b = _b.toLowerCase();
        }

        if (_a < _b) {
            return -1;
        } else if (_a > _b) {
            return 1;
        }
        return 0;
    }

    for (i = 0; i < ths.length; i++) {
        ths[i].addEventListener('click', function () {
            //console.log(this.cellIndex);

            //tbody直下のtr要素を取得する。querrySelectorAllで取得(Nodelist型)。
            //Array.prototype.slice.callで配列の型に変換
            var rows = Array.prototype.slice.call(document.querySelectorAll('tbody > tr'));

            //td要素内のクリックされた列番目を格納
            var col = this.cellIndex;

            var type = this.dataset.type;//string or number

            rows.sort(function (a, b) {
                return compare(a, b, col, type) * sortOrder;
            });

            rebuildtbody(rows);

            updateClassName(this);

            sortOrder *= -1;

        });
    }

})();