﻿//popup.js
/**
 * 
 * ※参考サイト（http://blog.magical-remix.net/igalog/archives/176）
 * 
 * <a href="http://www.yahoo.co.jp" rel="300,500" class="popup">ウィンドウ01</a>
 * 
 * rel="幅, 高さ"
 * 
*/

window.onload = function () {
  var node_a = document.getElementsByTagName('a');
  for (var i in node_a) {
    if (node_a[i].className == 'popup') {
      node_a[i].onclick = function () {
        return winOpen(this.href, this.rel)
      };
    }
  }

  picChangeName();
};

function winOpen(url, rel) {
  var split = rel.split(',');
  window.open(
    url, 'popup',
    'width=' + split[0] + ',height=' + split[1] + ',toolbar=no,location=no,status=no,menubar=no,scrollbars=yes,resizable=yes');

  return false;
};


//季節によってTop画像を変更する関数
function picChangeName() {

  let picname;
  const day = new Date;
  const month = day.getMonth() + 1;

  if (between(3, 5)) {
    picname = "main-haru";
  }
  else if (between(6, 8)) {
    picname = "main_summer01";
  }
  else if (between(9, 11)) {
    picname = "main-aki";
  }
  else if (12 == month || between(1, 2)) {
    picname = "main-fuyu";
  }
  else {
    picname = "main-haru";
  }

  const img_element = '<img src="./img/' + picname + '.jpg" width="584" height="452" alt="新車そっくり君" border="0"></img>'

  document.getElementById('img_js').insertAdjacentHTML("afterbegin", img_element);

  //月範囲
  function between(min, max) {
    return min <= month && month <= max
  }

}






///////////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * scrollsmoothly.js
 * Copyright (c) 2008 KAZUMiX
 * http://d.hatena.ne.jp/KAZUMiX/20080418/scrollsmoothly
 * Licensed under the MIT License:
 * http://www.opensource.org/licenses/mit-license.php
 * 
 * 更新履歴
 * 2009/02/12
 * スクロール先が画面左上にならない場合の挙動を修正
 * 2008/04/18
 * 公開
 *
*/

(function () {
  var easing = 0.5;
  var interval = 5;
  var d = document;
  var targetX = 0;
  var targetY = 0;
  var targetHash = '';
  var scrolling = false;
  var splitHref = location.href.split('#');
  var currentHref_WOHash = splitHref[0];
  var incomingHash = splitHref[1];
  var prevX = null;
  var prevY = null;

  // ドキュメント読み込み完了時にinit()を実行する
  addEvent(window, 'load', init);

  // ドキュメント読み込み完了時の処理
  function init() {
    // ページ内リンクにイベントを設定する
    setOnClickHandler();
    // 外部からページ内リンク付きで呼び出された場合
    if (incomingHash) {
      if (window.attachEvent && !window.opera) {
        // IEの場合はちょっと待ってからスクロール
        setTimeout(function () { scrollTo(0, 0); setScroll('#' + incomingHash); }, 50);
      } else {
        // IE以外はそのままGO
        scrollTo(0, 0);
        setScroll('#' + incomingHash);
      }
    }
  }

  // イベントを追加する関数
  function addEvent(eventTarget, eventName, func) {
    if (eventTarget.addEventListener) {
      // モダンブラウザ
      eventTarget.addEventListener(eventName, func, false);
    } else if (window.attachEvent) {
      // IE
      eventTarget.attachEvent('on' + eventName, function () { func.apply(eventTarget); });
    }
  }

  function setOnClickHandler() {
    var links = d.links;
    for (var i = 0; i < links.length; i++) {
      // ページ内リンクならスクロールさせる
      var link = links[i];
      var splitLinkHref = link.href.split('#');
      if (currentHref_WOHash == splitLinkHref[0] && d.getElementById(splitLinkHref[1])) {
        addEvent(link, 'click', startScroll);
      }
    }
  }

  function startScroll(event) {
    // リンクのデフォルト動作を殺す
    if (event) { // モダンブラウザ
      event.preventDefault();
      //alert('modern');
    } else if (window.event) { // IE
      window.event.returnValue = false;
      //alert('ie');
    }
    // thisは呼び出し元になってる
    setScroll(this.hash);
  }

  function setScroll(hash) {
    // ハッシュからターゲット要素の座標をゲットする
    var targetEle = d.getElementById(hash.substr(1));
    if (!targetEle) return;
    //alert(scrollSize.height);
    // スクロール先座標をセットする
    var ele = targetEle;
    var x = 0;
    var y = 0;
    while (ele) {
      x += ele.offsetLeft;
      y += ele.offsetTop;
      ele = ele.offsetParent;
    }
    var maxScroll = getScrollMaxXY();
    targetX = Math.min(x, maxScroll.x);
    targetY = Math.min(y, maxScroll.y);
    targetHash = hash;
    // スクロール停止中ならスクロール開始
    if (!scrolling) {
      scrolling = true;
      scroll();
    }
  }

  function scroll() {
    var currentX = d.documentElement.scrollLeft || d.body.scrollLeft;
    var currentY = d.documentElement.scrollTop || d.body.scrollTop;
    var vx = (targetX - currentX) * easing;
    var vy = (targetY - currentY) * easing;
    var nextX = currentX + vx;
    var nextY = currentY + vy;
    if ((Math.abs(vx) < 1 && Math.abs(vy) < 1)
      || (prevX === currentX && prevY === currentY)) {
      // 目標座標付近に到達していたら終了
      scrollTo(targetX, targetY);
      scrolling = false;
      location.hash = targetHash;
      prevX = prevY = null;
      return;
    } else {
      // 繰り返し
      scrollTo(parseInt(nextX), parseInt(nextY));
      prevX = currentX;
      prevY = currentY;
      setTimeout(function () { scroll() }, interval);
    }
  }

  function getDocumentSize() {
    return { width: Math.max(document.body.scrollWidth, document.documentElement.scrollWidth), height: Math.max(document.body.scrollHeight, document.documentElement.scrollHeight) };
  }

  function getWindowSize() {
    var result = {};
    if (window.innerWidth) {
      var box = d.createElement('div');
      with (box.style) {
        position = 'absolute';
        top = '0px';
        left = '0px';

        width = '100%';
        height = '100%';
        margin = '0px';
        padding = '0px';
        border = 'none';
        visibility = 'hidden';
      }
      d.body.appendChild(box);
      var width = box.offsetWidth;
      var height = box.offsetHeight;
      d.body.removeChild(box);
      result = { width: width, height: height };
    } else {
      result = { width: d.documentElement.clientWidth || d.body.clientWidth, height: d.documentElement.clientHeight || d.body.clientHeight };
    }
    return result;
  }

  function getScrollMaxXY() {
    if (window.scrollMaxX && window.scrollMaxY) {
      return { x: window.scrollMaxX, y: window.scrollMaxY };
    }
    var documentSize = getDocumentSize();
    var windowSize = getWindowSize();
    return { x: documentSize.width - windowSize.width, y: documentSize.height - windowSize.height };
  }

}());

///////////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * クロスフェードするロールオーバー処理
 * rollover2.js
 * Copyright (c) 2007 KAZUMiX
 * http://d.hatena.ne.jp/KAZUMiX/20071017/rollover2
 * 
 * Licensed under the MIT License:
 * http://www.opensource.org/licenses/mit-license.php
 * 
 * 更新履歴
 * 2009/06/02 ブラウザの「戻る」でフェード状態が残っている場合に対処
 * 2007/10/17 公開
 */

(function () {
  // ページ遷移時に透明にする用
  var rolloverImages = [];

  function setRollOver2() {
    if (!document.images) { return; }
    var imgs = document.images;
    var insert = [];
    for (var i = 0, len = imgs.length; i < len; i++) {
      var splitname = imgs[i].src.split('_off.');
      if (splitname[1]) {
        var rolloverImg = document.createElement('img');
        rolloverImages.push(rolloverImg);
        rolloverImg.src = splitname[0] + '_on.' + splitname[1];
        var alpha = 0;
        rolloverImg.currentAlpha = alpha;
        rolloverImg.style.opacity = alpha / 100;
        rolloverImg.style.filter = 'alpha(opacity=' + alpha + ')';
        rolloverImg.style.position = 'absolute';

        //ロールオーバー・アウト処理それぞれを設定
        addEvent(rolloverImg, 'mouseover', function () { setFader(this, 100); });
        addEvent(rolloverImg, 'mouseout', function () { setFader(this, 0); });

        // 後で追加するために追加場所と共に保存しておく
        // この時点で追加するとdocument.imagesが書き換わって不都合
        insert[insert.length] = { position: imgs[i], element: rolloverImg };
      }
    }
    // ↑で作ったロールオーバー画像を追加
    for (i = 0, len = insert.length; i < len; i++) {
      var parent = insert[i].position.parentNode;
      parent.insertBefore(insert[i].element, insert[i].position);
    }

    // ページ遷移時にはフェード状態をクリアする
    addEvent(window, 'beforeunload', clearRollover);
  }

  // 指定要素を指定透明度にするためのフェードアニメを設定する関数

  function setFader(targetObj, targetAlpha) {
    targetObj.targetAlpha = targetAlpha;
    if (targetObj.currentAlpha == undefined) {
      targetObj.currentAlpha = 100;
    }
    if (targetObj.currentAlpha == targetObj.targetAlpha) {
      return;
    }
    if (!targetObj.fading) {
      if (!targetObj.fader) {
        targetObj.fader = fader;
      }
      targetObj.fading = true;
      targetObj.fader();
    }
  }

  // アルファ値をターゲット値に近づける関数
  // ターゲット値になったら終了

  function fader() {
    this.currentAlpha += (this.targetAlpha - this.currentAlpha) * 0.2;
    if (Math.abs(this.currentAlpha - this.targetAlpha) < 1) {
      this.currentAlpha = this.targetAlpha;
      this.fading = false;
    }
    var alpha = parseInt(this.currentAlpha);
    this.style.opacity = alpha / 100;
    this.style.filter = 'alpha(opacity=' + alpha + ')';
    if (this.fading) {
      var scope = this;
      setTimeout(function () { fader.apply(scope) }, 30);
    }
  }

  // すべてのロールオーバー画像をを透明にする関数（遷移時用）

  function clearRollover() {
    for (var i = 0, len = rolloverImages.length; i < len; i++) {
      var image = rolloverImages[i];
      image.style.opacity = 0;
      image.style.filter = 'alpha(opacity=0)';
    }
  }

  // イベントを追加する関数

  function addEvent(eventTarget, eventName, func) {
    if (eventTarget.addEventListener) {
      // モダンブラウザ
      eventTarget.addEventListener(eventName, func, false);
    } else if (window.attachEvent) {
      // IE
      eventTarget.attachEvent('on' + eventName, function () { func.apply(eventTarget); });
    }
  }

  addEvent(window, 'load', setRollOver2);




})();


