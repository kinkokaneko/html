
  "use strict";
  {
    const open = document.getElementById("open");
    const close = document.getElementById("close");
    const modal = document.getElementById("modal");
    const mask = document.getElementById("mask");

    open.addEventListener("click", () => {
      modal.className = "";
      mask.className = "";
    });

    close.addEventListener("click", () => {
      modal.className = "hidden";
      mask.className = "hidden";
    });
    mask.addEventListener("click", () => {
      close.click();

    });

    document.getElementById("date").textContent = "buchiru";

    const now = new Date();
    const year = now.getFullYear();
    const month = now.getMonth();

    const date = now.getDate();
    const hour = now.getHours();
    const min = now.getMinutes();
    const sec = now.getSeconds();

    const output =
        year + "/" + (month + 1) + "/" + date + " " + hour + ":" + min + ":" + sec;

    document.getElementById("date").textContent = output;

  }