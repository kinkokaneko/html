
$(function () {

    const $list = $('.js_productContact_list');
    const $service = $('.js_productContact_services').hide();
    const serviceRequiredList = $service.data('list').split(',');

    $list.on('change.service', function () {
        const isShow = ($.inArray(this.value, serviceRequiredList) > -1);
        $service.toggle(isShow);


        if ($('.js_productContact_services').is(':visible')) {
            $('.js_productContact_services input').attr('data-validation-engine', 'validate[minCheckbox[1]]');
        } else {
            $('.js_productContact_services input').removeAttr('data-validation-engine');
        }
    }).triggerHandler('change.service');


    let value;
    let contact;

    $('#input-services-4').change(function (e) {

        //チェックボックスの状態参照(true,false)
        value = e.target.checked;

        if (value === true && contact == "導入検討のお客様") {
            $('.sf_list').show();

        } else {
            $('.sf_list').hide();
        }
    });

    $('#contact').change(function () {

        contact = $(this).val();

        if (value === true && contact == "導入検討のお客様") {
            $('.sf_list').show();

        } else {
            $('.sf_list').hide();
        }

    });

    $('.js_productContact_validate').validationEngine(
        'attach', { promptPosition: "topLeft" }
    );

});
