{
    'use strict';
    //クラス
    class Player{
        constructor(name,score){
            this.name=name;
            this.score=score;
        }


        //メソッド
        showInfo(){
            console.log(`name:${this.name},score:${this.score}`);//テンプレートリテラル
        }

        static showversion(){
            console.log('player class')
        }

    }

    //継承
    class SoccerPlayer extends Player{
        constructor(name,score,number){
            super(name,score);
            this.number=number;
        }

        kick(){
            console.log('Gooooooal');
        }


    }

    const kaneko=new Player('kaneko',32);
    const kimura=new Player('kimura',32);

    const tubasa=new SoccerPlayer('tubasa',99,10);
    tubasa.kick();

    kaneko.showInfo();
    kimura.showInfo();

    Player.showversion();


    //配列・forEach

    const a=[1,5,10];

    a.forEach(item=>{
        console.log(item);
    })

    //map・filter

    //配列内の要素をすべて二倍にして表示する(map)
    // const b=a.map(item=>item*2);
    // console.log(b);

    //配列内の要素から偶数だけ取り出す(filter)
    const b=a.filter(item=>item%2===0);
    console.log(b);

    //スプレッド演算子 ...c
    //配列の中身をコピー
    const c=[10,20];
    const sum=(a,b)=>a+b;
    console.log(sum(...c));

    //分割代入
    const numbers=[1,2,3,8];
    const[d,e,...rest]=numbers;
    console.log(d);
    console.log(e);
    console.log(rest);

    //分割代入＆オブジェクト&スプレッド演算子
    const buchiru={
        name:'taguchi',
        score:55,
        hp:33,
        mp:2
};
    const{name,score,...points}=buchiru;
    console.log(name);
    console.log(score);
    console.log(points);

    //Math
    const ran=Math.floor((Math.random()*6+1));
    console.log(ran);

    //Dateオブジェクト
    const p=new Date();

    console.log(p);
    console.log(p.getFullYear());
    console.log(p.getMonth());//月 0-11
    console.log(p.getDate());//日にち
    console.log(p.getDay());//曜日 0-6
    console.log(p.getHours());
    console.log(p.getMinutes());
    console.log(p.getSeconds());

    //windowオブジェクト
   // window.alert('hello');
   //  const ok=confirm('Are you sure?');
   //  console.log(ok);

    //setInterval(関数,ミリ秒);関数をミリ秒ごとに実行する

    let i=0;

    const showtime=()=>{
        console.log(new Date());
        i++;
        if(i>5){
        clearInterval((timerId));}

    };
    let timerId=setInterval(showtime,1000);





}