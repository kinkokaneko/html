LoRaWANによるIoTハンズオン技術セミナー
https://iotlabo.connpass.com/event/136862/
Iotハンズオン

## IoTの基礎
石山さん

### IoTについて
https://drive.google.com/file/d/1hjSvqnLgfEXD0eG3_1kNNoivAP0iJYDF/view

### IoT演習
Arduinoで温度計測
https://drive.google.com/file/d/1PoPKcdmpaF1wtKQAkfu3Q3c28vle9yMC/view



## IoT無線技術
### IoT無線技術概論
　https://drive.google.com/file/d/1r2RehdeTyyJ3UJU2T_49WETzTIEzXohg/

IoTで使われる無線技術とLoRaWANの概要
### IoT無線演習
Arduinoプログラムを使い、センサーデータをLoRaWANシールドからクラウドに飛ばす実習

### LPWAとは
LowPowerWideArea

種類
LoRaWAN
LTE-M(ソラコム)



## IoTクラウド演習
### IoTクラウド概論
https://drive.google.com/file/d/1HlvemtqhJjdr6UJMWoWPhDMwXn8lotCb/view?usp=sharing
### IoTクラウド演習
IoT/LPWAハンズオン・技術セミナー データの可視化
https://hackmd.io/@r5xCRJU6TZi9ZXEnaLJp2A/SyF4ydnfE

