
> **CMSとは**
> 
> [CMSとは](https://www.seohacks.net/basic/terms/cms/)
> [7.オープンソースと独自CMSの比較 - CMSとは？Webリニューアルのタイミングで導入したい更新システム！≪導入事例6社≫](https://mtame.jp/marketing_foundation/about_cms#a07)
> 
> アリスタイルでは、「WordPress」と「MovableType」をベースにしてお客さんごとにカスタマイズを行い、サイトの構築を行うことが多いです。
> （オーダーメイドのホームページというイメージです）
> 
> その他のCMSについても、お客さんから要望があれば調査を行いつつ作業していくことが多いです。
> 
> **WordPressとは**
> 
> [WordPressとは？](https://wp-exp.com/blog/wordpress/)
> [WordPressとは？初心者でも分かる基本機能とメリット・デメリット](https://digital-marketing.jp/creative/what-is-wordpress/)
> 
> **MovableTypeとは**
> 
> [Movable Typeとは何？ - Weblio辞書](https://www.weblio.jp/content/Movable+Type)
> [MovableTypeの公式サイト](https://www.sixapart.jp/movabletype/)
> [Movable Type の主な機能・特長](https://www.sixapart.jp/movabletype/features/)
> 
> このCMS研修では、「WordPress」のインストールからテンプレートファイルの作成までを行っていただきます。


## 「WordPress」と「MovableType」についての補足です

- WPのドキュメント  
https://wpdocs.osdn.jp/Main_Page
- MTのドキュメント  
https://www.movabletype.jp/documentation/

## テストサーバーの作り方です（アリスタイルの場合）

『@開発部 > Wiki > `*.test.alistyle.jp ( *.testsv.biz )`』
https://redmine.alistyle.info/redmine/projects/as_dev/wiki/Plesk

`/共有/20_管理部/00_サーバー情報/テストサーバー/テストサーバーの準備方法.pdf`

・テストサーバーのFTPの情報です。
`共有/20_管理部/00_サーバー情報/テストサーバー/情報.txt`




## Advanced Custom Fieldsの使い方
http://kotori-blog.com/wordpress/advanced-custom-fields/


## Movable Type について

[CMSやフレームワークのデモサイト - 開発部Wiki](https://redmine.alistyle.info/redmine/projects/as_dev/wiki/CMS・FrameWork_DEMO)
[Movable Type 7 マニュアル](https://www.movabletype.jp/documentation/)