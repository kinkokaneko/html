<?php get_header(); ?>

    <div class="container">
      <div class="main">
        <div class="row">
          <div class="col-md-9">
            <!-- レシピ090 -->
            <h2>スラッグが「movie」というカテゴリの名前</h2>
            <?php
              $cat = get_category_by_slug('movie');
              if ($cat) : ?>
              <p><?php echo $cat->name; ?></p>
            <?php else : ?>
              <p>スラッグが「movie」というカテゴリはありません。</p>
            <?php endif; ?>

            <!-- レシピ091 -->
            <h2>投稿が多いカテゴリベスト10</h2>
            <ul>
            <?php
              $cats = get_categories(array(
                'orderby' => 'count',
                'order' => 'desc',
                'number' => 10
              ));
              foreach ($cats as $cat) :
            ?>
              <li><a href="<?php echo get_category_link($cat->term_id); ?>"><?php echo $cat->name . '(' . $cat->count . ')'; ?></a></li>
            <?php endforeach; ?>
            </ul>

            <!-- レシピ097 -->
            <h2>スラッグが「hobby」というタグの名前</h2>
            <?php
              $tag = get_term_by('slug', 'hobby', 'post_tag');
              if ($tag) : ?>
              <p><?php echo $tag->name; ?></p>
            <?php else : ?>
              <p>スラッグが「hobby」というタグはありません。</p>
            <?php endif; ?>

            <!-- レシピ098 -->
            <h2>投稿が多いタグベスト10</h2>
            <ul>
            <?php
              $tags = get_tags(array(
                'orderby' => 'count',
                'order' => 'desc',
                'number' => 10
              ));
              foreach ($tags as $tag) :
            ?>
              <li><a href="<?php echo get_tag_link($tag->term_id); ?>"><?php echo $tag->name . '(' . $tag->count . ')'; ?></a></li>
            <?php endforeach; ?>
            </ul>

            <!-- レシピ094 -->
            <?php
              $cats = get_categories(array('parent' => 0));
              foreach ($cats as $cat) :
            ?>
              <h2><a href="<?php echo get_category_link($cat->term_id); ?>"><?php echo
            $cat->name; ?></a></h2>
              <p><?php echo category_description($cat->term_id); ?></p>
              <?php
                $q = new WP_Query(array('cat' => $cat->term_id));
                if ($q->have_posts ( )) :
              ?>
              <ul>
              <?php while ($q->have_posts ( )) : $q->the_post ( ); ?>
                <li><a href="<?php the_permalink ( ); ?>"><?php the_title ( ); ?></a></li>
              <?php endwhile; ?>
              </ul>
              <?php else: ?>
                <p>投稿がありません</p>
              <?php endif; ?>
            <?php endforeach; ?>
            
          </div>
          <div class="col-md-3">
            <?php get_sidebar(); ?>
          </div>
        </div>
      </div>

<?php get_footer(); ?>
