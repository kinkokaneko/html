<?php if ( have_posts() ) : ?>
  <?php while ( have_posts() ) : the_post(); ?>
    <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
      <?php if (is_singular()) : ?>
        <?php the_title('<h2>', '</h2>'); ?>
      <?php else : ?>
        <?php $permalink = get_permalink(); ?>
        <?php the_title('<h2><a href="' . $permalink . '">', '</a></h2>'); ?>
      <?php endif; ?>
      <div class="post-meta">
        <p class="post-date">日付:<?php the_date(); ?></p>
        <!-- レシピ089,093 -->
        <div class="post-categories">カテゴリー:
        <?php
          $cats = get_the_category ( );
          $cat_count = count($cats);
          for ($i = 0; $i < $cat_count; $i++) :
        ?>
          <?php if ($i == 0) : ?><ul><?php endif; ?>
          <li><a href="<?php echo get_category_link($cats[$i]->term_id); ?>"><?php echo $cats[$i]->name; ?></a></li>
          <?php if ($i == $cat_count - 1) : ?></ul><?php endif; ?>
        <?php endfor; ?>
        </div>
        <!-- レシピ096,099 -->
        <?php
          $tags = get_the_tags();
          if ($tags) :
        ?>
        <div class="post-tags">タグ:
        <?php
            $tag_count = count($tags);
            for ($i = 0; $i < $tag_count; $i++) :
        ?>
          <?php if ($i == 0) : ?><ul><?php endif; ?>
          <li>
<a href="<?php echo get_tag_link($tags[$i]->term_id); ?>">
<?php echo $tags[$i]->name; ?>
</a></li>
          <?php if ($i == $tag_count - 1) : ?></ul><?php endif; ?>
        <?php endfor; ?>
        </div>
        <?php endif; ?>
      </div>
      <div class="post-content">
        <?php the_content(); ?>
      </div>
      <?php if (is_singular()) : ?>
        <?php comments_template(); ?>
      <?php endif; ?>
    </article>
  <?php endwhile; ?>
  <div class="paginate-links"><?php echo paginate_links(); ?></div>
<?php else : ?>
  <p>投稿がありません。</p>
<?php endif; ?>
