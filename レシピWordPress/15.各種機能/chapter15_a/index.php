<?php get_header(); ?>

    <div class="container">
      <div class="main">
        <div class="row">
          <div class="col-md-9">
            <h2>esc_html関数</h2>
            <?php
              $text = '<test>&\'"</test>';
              /* レシピ237 */
              echo esc_html($text);
            ?>

            <h2>esc_attr関数</h2>
            <?php
              $text = '<test>&\'"</test>';
            ?>
            <!-- レシピ238 -->
            <p><a href="#" title="<?php echo esc_attr($text); ?>">esc_attr関数</a></p>

            <h2>esc_url関数</h2>
            <?php 
              $url = "javascript:alert('abc');";
            ?>
            <!-- レシピ239 -->
            <p><a href="<?php echo esc_url($url); ?>">esc_url関数</a></p>

            <h2>esc_js関数</h2>
            <p id="esc_js"></p>
            <?php
              $text = 'hello world';
            ?>
            <script type="text/javascript">
              jQuery(function() {
                var text;
                // レシピ240
                text = '<?php echo esc_js($text); ?>';
                jQuery('#esc_js').html(text);
              });
            </script>

            <h2>esc_textarea関数</h2>
            <?php
              $text = <<<HERE
hello world
<script type="text/javascript">
var text = 'test';
</script>
HERE;
            ?>
            <!-- レシピ241 -->
            <textarea><?php echo esc_textarea($text); ?></textarea>

            <h2>wp_kses関数</h2>
            <?php
              $html = <<<HERE
<p class="test">
<a href="http://www.h-fj.com/blog/" title="http link" target="_blank">httpのリンク</a><br />
<a href="https://www.h-fj.com/blog/">httpsのリンク</a><br />
<a href="mailto:foo@sample.com">メールアドレスへのリンク</a><br />
<img src="http://www.h-fj.com/pic/hf.png" />
</p>
HERE;
              /* レシピ242 */
              $allowed_html = array(
                'a' => array(
                  'href' => array(),
                  'target' => array()
                ),
                'p' => array(),
                'br' => array()
              );
              $allowed_protocols = array('http');
              echo wp_kses($html, $allowed_html, $allowed_protocols);
            ?>

            <h2>wp_get_shortlink関数</h2>
            <!-- レシピ243 -->
            <a href="<?php echo wp_get_shortlink(1); ?>">IDが1番の投稿</a>
          </div>
          <div class="col-md-3">
            <?php get_sidebar(); ?>
          </div>
        </div>
      </div>

<?php get_footer(); ?>
