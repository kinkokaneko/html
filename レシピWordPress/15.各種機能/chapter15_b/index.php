<?php
  $app_id = "アプリケーションID";
?>
<?php get_header(); ?>

    <div class="container">
      <div class="main">
        <div class="row">
          <div class="col-md-9">
            <h2>wp_remote_get関数</h2>
            <ul>
            <?php
              /* レシピ244 */
              $url = "http://shopping.yahooapis.jp/ShoppingWebService/V1/json/itemSearch?appid={$app_id}&query=WordPress";
              $response = wp_remote_get($url);
              $objs = json_decode($response['body']);
              $items = $objs->ResultSet->{0}->Result;
              for ($i = 0; $i < 10; $i++) : $item = $items->{$i};
            ?>
              <li><?php echo $item->Name; ?></li>	
            <?php endfor; ?>
            </ul>

            <h2>wp_remote_post関数</h2>
            <ul>
            <?php
              /* レシピ245 */
              $url = 'http://jlp.yahooapis.jp/FuriganaService/V1/furigana';
              $params = array(
                'httpversion' => '1.1',
                'user-agent' => "Yahoo AppID: {$app_id}",
                'body' => array(
                  'sentence' => '日本語の文章'
                )
              );
              $response = wp_remote_post($url, $params);
              $xml = new SimpleXMLElement($response['body']);
              $words = $xml->Result->WordList->Word;
              for ($i = 0; $i < count($words); $i++) : $word = $words[$i];
            ?>
              <li><?php echo $word->Surface; ?>(<?php echo $word->Furigana; ?>)</li>
            <?php endfor; ?>
            </ul>

          </div>
          <div class="col-md-3">
            <?php get_sidebar(); ?>
          </div>
        </div>
      </div>

<?php get_footer(); ?>
