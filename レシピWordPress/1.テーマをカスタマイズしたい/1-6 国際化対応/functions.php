<?php
/* テーマの設定(レシピ024) */
function add_theme_support_cb() {
/*  add_theme_support('custom-background', ・・・);
  add_theme_support('custom-header', ・・・);
  register_default_headers(・・・);
  add_theme_support('post-formats', ・・・);
  register_nav_menus(・・・);*/
  add_theme_support('title-tag');
  load_theme_textdomain('chapter01_f', get_template_directory ( ) . '/languages');
}
add_action('after_setup_theme', 'add_theme_support_cb');
/* スタイルシートとJavaScriptの組み込み(レシピ197/198参照) */
function enqueue_cb() {
	wp_enqueue_script('jquery');
	wp_enqueue_style('bootstrap', get_stylesheet_directory_uri() . '/css/bootstrap.min.css');
	wp_enqueue_style('mystyle', get_stylesheet_directory_uri() . '/css/style.css', 'bootstrap');
}
add_action('wp_enqueue_scripts', 'enqueue_cb');
/* 辞書ファイルの読み込み */

?>
