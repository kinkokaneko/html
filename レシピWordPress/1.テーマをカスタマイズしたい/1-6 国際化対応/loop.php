<?php if ( have_posts() ) : ?>
  <?php while ( have_posts() ) : the_post(); ?>
    <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
      <?php if (is_singular()) : ?>
        <?php the_title('<h2>', '</h2>'); ?>
      <?php else : ?>
        <?php $permalink = get_permalink(); ?>
        <?php the_title('<h2><a href="' . $permalink . '">', '</a></h2>'); ?>
      <?php endif; ?>
      <div class="post-meta">
        <!-- _e関数の例 -->
        <p class="post-date"><?php _e('Date', 'chapter01_f'); ?>:<?php the_date(); ?></p>
        <div class="post-categories"><?php _e('Category', 'chapter01_f'); ?>:<?php the_category(); ?></div>
        <div class="post-tags"><?php the_tags(); ?></div>
      </div>
      <div class="post-content">
        <?php the_content(); ?>
      </div>
      <?php if (is_singular()) : ?>
        <?php comments_template(); ?>
      <?php endif; ?>
    </article>
  <?php endwhile; ?>
  <div class="paginate-links"><?php echo paginate_links(); ?></div>
<?php else : ?>
  <p><?php __e('No posts found.'); ?></p>
<?php endif; ?>
