<?php get_header(); ?>

    <div class="container">
      <div class="main">
        <div class="row">
          <div class="col-md-9">
            <?php
              $count_posts = wp_count_posts();
              $published_posts = $count_posts->publish;
            ?>
            <!-- __関数の例 -->
            <h2><?php echo sprintf(__('Total %d posts', 'chapter01_f'), $published_posts); ?></h2>
            <?php get_template_part('loop'); ?>
          </div>
          <div class="col-md-3">
            <?php get_sidebar(); ?>
          </div>
        </div>
      </div>

<?php get_footer(); ?>
