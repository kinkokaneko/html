<?php get_header(); ?>

    <!-- 投稿用のテンプレート -->
    <div class="container">
      <div class="main">
        <div class="row">
          <div class="col-md-9">
            <!-- レシピ011 -->
            <?php get_template_part('loop', 'single'); ?>
          </div>
          <div class="col-md-3">
            <?php get_sidebar(); ?>
          </div>
        </div>
      </div>

<?php get_footer(); ?>
