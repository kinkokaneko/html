<table class="table">
  <tr><th>機能</th><th>対応/未対応</th></tr>
<?php
$supports = array('post-thumbnails', 'post-formats', 'custom-header', 'custom-background', 'menus', 'automatic-feed-links', 'editor-style', 'widgets', 'html5', 'title-tag');
foreach ($supports as $support) :
?>
  <tr>
    <td><?php echo $support; ?></td>
    <td>
    <?php if (current_theme_supports($support)) : ?>
      未対応
    <?php else : ?>
      対応
    <?php endif; ?>
    </td>
  </tr>
<?php endforeach; ?>
</table>