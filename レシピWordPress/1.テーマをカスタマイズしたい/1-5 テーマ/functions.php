<?php
function header_style() {
?>
<style type="text/css">
  div.header {
    background-image: url(<?php header_image(); ?>);
    height: <?php echo get_custom_header()->height; ?>px;
    width: <?php echo get_custom_header()->width; ?>px;
  }
</style>
<?php
}
/* テーマの設定 */
function add_theme_support_cb() {
  /* カスタム背景(レシピ017) */
  add_theme_support('custom-background');
  $args = array(
    'width'  => 800,
    'height' => 100,
    'default-image' => '%s/headers/header1.jpg',
    'wp-head-callback' => 'header_style',
  );
  /* カスタムヘッダー(レシピ018,019,020) */
  add_theme_support('custom-header', $args);
  register_default_headers(array(
    'header1' => array(
      'url' => '%s/headers/header1.jpg',
      'thumbnail_url' => '%s/headers/header1.jpg'
    ),
    'header2' => array(
      'url' => '%s/headers/header2.jpg',
      'thumbnail_url' => '%s/headers/header2.jpg'
    ),
    'header3' => array(
      'url' => '%s/headers/header3.jpg',
      'thumbnail_url' => '%s/headers/header3.jpg'
    ),
  ));

  /* カスタムメニュー(レシピ021) */
  /*  register_nav_menu('main-menu', 'メインメニュー');*/
  register_nav_menus(array(
    'main-menu' => 'メインメニュー',
    'sub-menu' => 'サブメニュー'
  ));
  /* 投稿フォーマット(レシピ022) */
  add_theme_support('post-formats', array('gallery', 'image', 'video', 'audio'));
  /* タイトルタグ(レシピ023) */
  add_theme_support('title-tag');
}
add_action('after_setup_theme', 'add_theme_support_cb');

/* ビジュアルエディタ用スタイル(レシピ027) */
function add_editor_style_cb ( ) {
  add_editor_style ( );
}
add_action('admin_init', 'add_editor_style_cb');

/* スタイルシートとJavaScriptの組み込み(レシピ197/198参照) */
function enqueue_cb() {
	wp_enqueue_script('jquery');
	wp_enqueue_style('bootstrap', get_stylesheet_directory_uri() . '/css/bootstrap.min.css');
	wp_enqueue_style('mystyle', get_stylesheet_directory_uri() . '/css/style.css', 'bootstrap');
}
add_action('wp_enqueue_scripts', 'enqueue_cb');
?>
