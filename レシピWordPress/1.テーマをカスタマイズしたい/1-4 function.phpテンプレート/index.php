<?php get_header(); ?>

    <div class="container">
      <div class="main">
        <div class="row">
          <div class="col-md-9">
            <?php if (is_child_theme ( )) : ?>
              <h2>子テーマです</h2>
            <?php else : ?>
              <h2>子テーマはありません</h2>
            <?php endif; ?>
            <?php get_template_part('loop'); ?>
          </div>
          <div class="col-md-3">
            <?php get_sidebar(); ?>
          </div>
        </div>
      </div>

<?php get_footer(); ?>
