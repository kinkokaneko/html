<h1>レシピ 072</h1>

<h2>日付の昇順で並べ替え</h2>
<?php read_posts_by_wp_query(array('orderby' => 'date', 'order' => 'ASC')); ?>
<hr />

<h2>priceカスタムフィールドの値の降順で並べ替え</h2>
<?php read_posts_by_wp_query(array('meta_key' => 'price', 'orderby' => 'meta_value_num', 'order' => 'DESC')); ?>
