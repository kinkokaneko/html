<?php get_header(); ?>

    <div class="container">
      <div class="main">
        <div class="row">
          <div class="col-md-9">
<?php
  $samples = array('065' => 1, '066' => 1, '067' => 1, '068' => 1, '069' => 1, '070' => 1, '071' => 1, '072' => 1, '073' => 1, '074' => 1, '075' => 1, '076' => 1);
  $sample_no = '';
  if (isset($_GET['sample'])) {
    $sample_no = $_GET['sample'];
  }
  if (isset($samples[$sample_no])) :
?>
            <?php get_template_part('sample_' . $sample_no); ?>
<?php else : ?>
            <?php get_template_part('loop'); ?>
<?php endif; ?>
          </div>
          <div class="col-md-3">
            <?php get_sidebar(); ?>
          </div>
        </div>
      </div>

<?php get_footer(); ?>
