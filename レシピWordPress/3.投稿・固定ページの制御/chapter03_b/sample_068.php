<h1>レシピ 068</h1>

<h2>2016年の投稿</h2>
<?php read_posts_by_wp_query(array('year' => 2016, 'posts_per_page' => -1)); ?>
<hr />

<h2>2015年末までの投稿</h2>
<?php
read_posts_by_wp_query(array(
  'date_query' => array(
    array(
      'year' => 2016,
      'compare' => '<'
    )
  ),
  'posts_per_page' => -1,
  'orderby' => 'date',
  'order' => 'DESC'
));
?>
<hr />

<h2>2015年7月から12月の投稿</h2>
<?php
read_posts_by_wp_query(array(
  'date_query' => array(
    array(
      'year' => 2015,
      'month' => 7,
      'compare' => '>='
    ),
    array(
      'year' => 2015,
      'month' => 12,
      'compare' => '<='
    )
  ),
  'posts_per_page' => -1,
  'orderby' => 'date',
  'order' => 'DESC'
));
?>
