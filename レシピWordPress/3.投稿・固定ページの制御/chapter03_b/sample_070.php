<h1>レシピ 070</h1>

<h2>priceカスタムフィールドの値が300以上の投稿</h2>
<?php read_posts_by_wp_query(array('meta_key' => 'price', 'meta_type' => 'NUMERIC', 'meta_value' => 300, 'meta_compare' => '>=')); ?>
<hr />

<h2>priceカスタムフィールドの値が1000から2000で、かつcolorカスタムフィールドの値にredかblueを含む投稿</h2>
<?php
  read_posts_by_wp_query(array(
    'meta_query' => array(
      'relation' => 'AND',
      array('key' => 'price', 'value' => array(1000, 2000), 'type' => 'NUMERIC', 'compare' => 'BETWEEN'),
      array(
        'relation' => 'OR',
        array('key' => 'color', 'value' => 'red', 'compare' => 'LIKE'),
        array('key' => 'color', 'value' => 'blue', 'compare' => 'LIKE')
      )
    )
  )); 
?>
