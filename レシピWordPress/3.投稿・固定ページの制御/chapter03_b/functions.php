<?php
/* テーマの設定 */
function add_theme_support_cb() {
/*  add_theme_support('custom-background', ・・・);
  add_theme_support('custom-header', ・・・);
  register_default_headers(・・・);
  add_theme_support('post-formats', ・・・);
  register_nav_menus(・・・);*/
  add_theme_support('title-tag');
}
add_action('after_setup_theme', 'add_theme_support_cb');
/* スタイルシートとJavaScriptの組み込み(レシピ197／198参照) */
function enqueue_cb() {
	wp_enqueue_script('jquery');
	wp_enqueue_style('bootstrap', get_stylesheet_directory_uri() . '/css/bootstrap.min.css');
	wp_enqueue_style('mystyle', get_stylesheet_directory_uri() . '/css/style.css', 'bootstrap');
}
add_action('wp_enqueue_scripts', 'enqueue_cb');

/* WP_Queryで投稿を読み込む */
function read_posts_by_wp_query($args) {
  global $post;
  $my_query = new WP_Query($args);
  $sample_no = $_GET['sample'];
  if ($my_query->have_posts()) :
?>
  <?php while ($my_query->have_posts()) : $my_query->the_post(); ?>
  <li><?php the_title(); ?><br />
  <?php if ($sample_no == 65 || $sample_no == 75 || $sample_no == 76) : ?>
    (ID = <?php the_ID(); ?>, スラッグ = <?php echo esc_html($post->post_name); ?>)
  <?php elseif ($sample_no == 66) : ?>
    (カテゴリー:<?php the_category(); ?>)
  <?php elseif ($sample_no == 67) : ?>
    (<?php the_tags(); ?>)
  <?php elseif ($sample_no == 68) : ?>
    (日付:<?php the_time('Y/m/d H:i:s'); ?>)
  <?php elseif ($sample_no == 69) : ?>
    (ユーザー:<?php the_author(); ?>)
  <?php elseif ($sample_no == 70 || $sample_no == 74) : ?>
    (price = 
    <?php
      $price = get_post_meta(get_the_ID(), 'price', true);
      echo $price;
    ?>, color = 
    <?php
      $colors = get_post_meta(get_the_ID(), 'color', false);
      for ($i = 0; $i < count($colors); $i++) {
        echo $colors[$i];
        if ($i < count($colors) - 1) {
          echo ', ';
        }
      }
    ?>)
  <?php elseif ($sample_no == 71) : ?>
    (ステータス:<?php echo $post->post_status; ?>)
  <?php elseif ($sample_no == 72) : ?>
    <?php if ($args['orderby'] == 'date') : ?>
      (日付:<?php the_time('Y/m/d H:i:s'); ?>)
    <?php elseif ($args['orderby'] == 'meta_value_num') : ?>
      (price:<?php $price = get_post_meta(get_the_ID(), 'price', true); echo $price; ?>)
    <?php endif; ?>
  <?php elseif ($sample_no == 73) : ?>
    (日付:<?php the_time('Y/m/d H:i:s'); ?>)
  <?php endif; ?>
  </li>
  <?php endwhile; ?>
<?php else: ?>
  <p>条件に合う投稿はありません。</p>
<?php
  endif; 
  wp_reset_postdata();
}
?>
