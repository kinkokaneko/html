<h1>レシピ 067</h1>

<h2>IDが11番のタグがついた投稿</h2>
<?php read_posts_by_wp_query(array('tag_id' => 11)); ?>
<hr />

<h2>IDが12番か13番のタグがついた投稿</h2>
<?php read_posts_by_wp_query(array('tag__in' => array(12, 13))); ?>
<hr />

<h2>スラッグが「foo」か「bar」のタグがついた投稿</h2>
<?php read_posts_by_wp_query(array('tag' => 'foo,bar')); ?>
<hr />

<h2>IDが12番と13番の両方のタグがついた投稿</h2>
<?php read_posts_by_wp_query(array('tag__and' => array(12, 13))); ?>
<hr />

<h2>スラッグが「foo」と「bar」の両方のタグがついた投稿</h2>
<?php read_posts_by_wp_query(array('tag' => 'foo+bar')); ?>

