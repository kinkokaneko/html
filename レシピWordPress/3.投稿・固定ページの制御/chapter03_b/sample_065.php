<h1>レシピ 065</h1>

<h2>IDが1番の投稿</h2>
<?php read_posts_by_wp_query(array('p' => 1)); ?>
<hr />

<h2>IDが1番か2番か3番の投稿</h2>
<?php read_posts_by_wp_query(array('post__in' => array(1, 2, 3))); ?>
<hr />

<h2>IDが1番か2番か3番でない投稿</h2>
<?php read_posts_by_wp_query(array('post__not_in' => array(1, 2, 3))); ?>
<hr />

<h2>スラッグが「sapmle」の投稿</h2>
<?php read_posts_by_wp_query(array('name' => 'sample')); ?>
<hr />

<h1>スラッグが「hello-world」か「sample」の投稿</h1>
<?php read_posts_by_wp_query(array('post_name__in' => array('hello-world', 'sample'))); ?>
