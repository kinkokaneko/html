<h1>レシピ 065</h1>

<h2>IDが1番のカテゴリーの投稿</h2>
<?php read_posts_by_wp_query(array('cat' => 1)); ?>
<hr />

<h2>IDが2番か3番のカテゴリーの投稿</h2>
<?php read_posts_by_wp_query(array('cat' => '2,3')); ?>
<hr />

<h2>スラッグが「foo」か「bar」のカテゴリーの投稿</h2>
<?php read_posts_by_wp_query(array('category_name' => 'foo,bar')); ?>
<hr />

<h2>IDが2番か3番のカテゴリーの投稿（子孫カテゴリーは除外）</h2>
<?php read_posts_by_wp_query(array('category__in' => array(2, 3))); ?>
<hr />

<h2>スラッグが「foo」と「bar」の両方のカテゴリーに属する投稿</h2>
<?php read_posts_by_wp_query(array('category_name' => 'foo+bar')); ?>
<hr />

<h2>IDが2番と3番の両方のカテゴリーに属する投稿</h2>
<?php read_posts_by_wp_query(array('category__and' => array(2, 3))); ?>
<hr />

<h2>IDが2番と3番のカテゴリーに属さない投稿</h2>
<?php read_posts_by_wp_query(array('cat' => '-2,-3')); ?>
<hr />

<h2>IDが2番と3番のカテゴリーに属さない投稿（子孫カテゴリーは除外しない）</h2>
<?php read_posts_by_wp_query(array('category__not_in' => array(2, 3))); ?>
<hr />