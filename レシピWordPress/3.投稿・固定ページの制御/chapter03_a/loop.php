<?php if ( have_posts() ) : ?>
  <?php while ( have_posts() ) : the_post(); ?>
    <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
      <?php if (is_singular()) : ?>
        <?php the_title('<h2>', '</h2>'); ?>
      <?php else : ?>
        <?php $permalink = get_permalink(); ?>
        <?php the_title('<h2><a href="' . $permalink . '">', '</a></h2>'); ?>
      <?php endif; ?>
      <div class="post-meta">
        <p class="post-date">日付:<?php the_date(); ?></p>
        <div class="post-categories">カテゴリー:<?php the_category(); ?></div>
        <div class="post-tags"><?php the_tags(); ?></div>
      </div>
      <div class="post-content">
        <?php the_content(); ?>
      </div>
      <?php if (is_singular()) : ?>
        <h2>sizeカスタムフィールドの値</h2>
        <ul>
	<!-- レシピ061 -->
        <?php
          $sizes = get_post_meta(get_the_ID(), 'size', false);
          foreach ($sizes as $size) :
        ?>
          <li><?php echo $size; ?></li>
        <?php endforeach; ?>
        </ul>

        <h2>カスタムフィールドの名前</h2>
        <ul>
	<!-- レシピ062 -->
        <?php
          $keys = get_post_custom_keys(get_the_ID());
          foreach ($keys as $key) :
          if (substr($key, 0, 1) != '_') :
        ?>
          <li><?php echo$key; ?></li>
        <?php endif; endforeach; ?>
        </ul>

        <h2>カスタムフィールドの値と名前</h2>
        <ul>
	<!-- レシピ063 -->
        <?php
          $cf = get_post_custom(get_the_ID());
          foreach ($cf as $key => $values) :
            if (substr($key, 0, 1) != '_') :
        ?>
          <li><?php echo $key; ?>
          <ul>
          <?php
            foreach ($values as $value) :
          ?>
            <li><?php echo $value; ?></li>
          <?php endforeach; ?>
          </ul>
        <?php endif; endforeach; ?>
        </ul>

        <?php comments_template(); ?>
      <?php endif; ?>
    </article>
  <?php endwhile; ?>
  <div class="paginate-links"><?php echo paginate_links(); ?></div>
<?php else : ?>
  <p>投稿がありません。</p>
<?php endif; ?>
