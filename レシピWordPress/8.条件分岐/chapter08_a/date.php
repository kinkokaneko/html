<?php
if (is_year() && !get_query_var('w')) {
	get_template_part('yearly');
}
else if (is_month()) {
	get_template_part('monthly');
}
else if (is_day()) {
	get_template_part('daily');
}
else {
	get_template_part('date-other');
}
?>
