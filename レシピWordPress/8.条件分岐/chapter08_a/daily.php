<?php get_header(); ?>

    <div class="container">
      <div class="main">
        <div class="row">
          <div class="col-md-9">
            <h1><?php global $wp_query; echo $wp_query->query_vars['year']; ?>年<?php echo $wp_query->query_vars['monthnum']; ?>月<?php echo $wp_query->query_vars['day']; ?>日</h1>
            <?php get_template_part('loop'); ?>
          </div>
          <div class="col-md-3">
            <?php get_sidebar(); ?>
          </div>
        </div>
      </div>

<?php get_footer(); ?>
