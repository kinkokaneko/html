<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="<?php esc_attr(bloginfo('description')); ?>">
    <meta name="author" content="">
    <link rel="icon" href="favicon.ico">
    <?php wp_head(); ?>
  </head>

  <body>

    <nav class="navbar navbar-fixed-top navbar-dark bg-inverse">
      <ul class="nav navbar-nav">
        <li class="nav-item active">
          <a class="nav-link" href="<?php echo home_url('/'); ?>">Home</a>
        </li>
      </ul>
    </nav>

    <div class="container">
      <div class="site-header">
        <div class="row">
          <div class="col-md-12">
            <?php if (is_home()) : ?>
              <h1><a href="<?php echo home_url('/'); ?>"><?php bloginfo('name') ?></a></h1>
            <?php else : ?>
              <div class="site-title"><a href="<?php echo home_url('/'); ?>"><?php bloginfo('name') ?></a></div>
            <?php endif; ?>
            <p><?php bloginfo('description'); ?></p>

            <h2>条件タグの動作</h2>
            <table class="table">
              <tr>
                <td>is_home()</td>
                <td>
                  <!-- レシピ130 -->
                  <?php if (is_home()) : ?>
                    ○
                  <?php else : ?>
                    ×
                  <?php endif; ?>
                </td>
              </tr>
              <tr>
                <td>is_front_page()</td>
                <td>
                  <!-- レシピ131 -->
                  <?php if (is_front_page ()) : ?>
                    ○
                  <?php else : ?>
                    ×
                  <?php endif; ?>
                </td>
              </tr>
              <tr>
                <td>is_single()</td>
                <td>
                  <!-- レシピ132 -->
                  <?php if (is_single()) : ?>
                    ○
                  <?php else : ?>
                    ×
                  <?php endif; ?>
                </td>
              </tr>
              <tr>
                <td>is_single(array(1, 2, 'foo', 'bar'))</td>
                <td>
                  <!-- レシピ132 -->
                  <?php if (is_single(array(1, 2, 'foo', 'bar'))) : ?>
                    ○
                  <?php else : ?>
                    ×
                  <?php endif; ?>
                </td>
              </tr>
              <tr>
                <td>is_page()</td>
                <td>
                  <!-- レシピ133 -->
                  <?php if (is_page()) : ?>
                    ○
                  <?php else : ?>
                    ×
                  <?php endif; ?>
                </td>
              </tr>
              <tr>
                <td>is_page(array(1, 2, 'foo', 'bar'))</td>
                <td>
                  <!-- レシピ133 -->
                  <?php if (is_page(array(1, 2, 'foo', 'bar'))) : ?>
                    ○
                  <?php else : ?>
                    ×
                  <?php endif; ?>
                </td>
              </tr>
              <tr>
                <td>is_date()</td>
                <td>
                  <!-- レシピ134 -->
                  <?php if (is_date()) : ?>
                    ○
                  <?php else : ?>
                    ×
                  <?php endif; ?>
                </td>
              </tr>
              <tr>
                <td>is_category()</td>
                <td>
                  <!-- レシピ136 -->
                  <?php if (is_category()) : ?>
                    ○
                  <?php else : ?>
                    ×
                  <?php endif; ?>
                </td>
              </tr>
              <tr>
                <td>is_category(array(1, 2, 'foo', 'bar'))</td>
                <td>
                  <!-- レシピ138 -->
                  <?php if (is_category(array(1, 2, 'foo', 'bar'))) : ?>
                    ○
                  <?php else : ?>
                    ×
                  <?php endif; ?>
                </td>
              </tr>
              <tr>
                <td>is_tag()</td>
                <td>
                  <!-- レシピ140 -->
                  <?php if (is_tag()) : ?>
                    ○
                  <?php else : ?>
                    ×
                  <?php endif; ?>
                </td>
              </tr>
              <tr>
                <td>is_tag(array(1, 2, 'foo', 'bar'))</td>
                <td>
                  <!-- レシピ140 -->
                  <?php if (is_tag(array('movie', 'music'))) : ?>
                    ○
                  <?php else : ?>
                    ×
                  <?php endif; ?>
                </td>
              </tr>
              <tr>
                <td>is_author()</td>
                <td>
                  <!-- レシピ142 -->
                  <?php if (is_author()) : ?>
                    ○
                  <?php else : ?>
                    ×
                  <?php endif; ?>
                </td>
              </tr>
              <tr>
                <td>is_author(1)</td>
                <td>
                  <!-- レシピ142 -->
                  <?php if (is_author(1)) : ?>
                    ○
                  <?php else : ?>
                    ×
                  <?php endif; ?>
                </td>
              </tr>
              <tr>
                <td>is_search()</td>
                <td>
                  <!-- レシピ143 -->
                  <?php if (is_search()) : ?>
                    ○
                  <?php else : ?>
                    ×
                  <?php endif; ?>
                </td>
              </tr>
              <tr>
                <td>is_404()</td>
                <td>
                  <!-- レシピ144 -->
                  <?php if (is_404()) : ?>
                    ○
                  <?php else : ?>
                    ×
                  <?php endif; ?>
                </td>
              </tr>
              <tr>
                <td>is_paged()</td>
                <td>
                  <!-- レシピ146 -->
                  <?php if (is_paged()) : ?>
                    ○
                  <?php else: ?>
                    ×
                  <?php endif; ?>
                </td>
              </tr>

            <?php if (is_category()) : ?>
              <tr>
                <td>cat_is_ancestor_of(10, $cat)／cat_is_ancestor_of(20, $cat)</td>
                <td>
                  <!-- レシピ138 -->
                  <?php $cat = get_queried_object ( ); if (cat_is_ancestor_of(10, $cat)) : ?>
                    IDが10番のカテゴリーの子（または孫以下）
                  <?php elseif (cat_is_ancestor_of(20, $cat)) : ?>
                    IDが20番のカテゴリーの子（または孫以下）
                  <?php else: ?>
                    IDが10番か20番のカテゴリの子（または孫以下）ではない
                  <?php endif; ?>
                </td>
              </tr>
           <?php endif; ?>

            <?php if (is_single()) : ?>
              <tr>
                <td>IDが1番のカテゴリーに属する（in_category(1)）</td>
                <td>
                  <!-- レシピ139 -->
                  <?php if (in_category(1)) : ?>
                    ○
                  <?php else: ?>
                    ×
                  <?php endif; ?>
                </td>
              </tr>
              <tr>
                <td>is_sticky()</td>
                <td>
                  <!-- レシピ145 -->
                  <?php if (is_sticky()) : ?>
                    ○
                  <?php else: ?>
                    ×
                  <?php endif; ?>
                </td>
              </tr>
              <tr>
                <td>投稿フォーマットがimage（get_post_format）</td>
                <td>
                  <!-- レシピ147 -->
                  <?php 
                    $post_format = get_post_format ();
                    if ($post_format == 'image') :
                  ?>
                    ○
                  <?php else: ?>
                    ×
                  <?php endif; ?>
                </td>
              </tr>
              <tr>
                <td>パスワード保護されている（post_password_required）</td>
                <td>
                  <!-- レシピ148 -->
                  <?php if (post_password_required ()) : ?>
                    ○
                  <?php else: ?>
                    ×
                  <?php endif; ?>
                </td>
              </tr>
           <?php endif; ?>

            </table>
          </div>
        </div>
      </div>
    </div>