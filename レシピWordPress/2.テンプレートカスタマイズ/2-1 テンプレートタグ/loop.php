<!-- レシピ038 WordPressループ -->
<?php if ( have_posts() ) : ?>
  <?php while ( have_posts() ) : the_post(); ?>
    <!-- レシピ039 post_class関数、レシピ040 the_id関数 -->
    <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
      <?php if (is_singular()) : ?>
        <!-- レシピ041 the_title関数 -->
        <?php the_title('<h2>', '</h2>'); ?>
      <?php else : ?>
        <!-- レシピ047 get_permalink関数 -->
        <?php $permalink = get_permalink(); ?>
        <!-- レシピ041 the_title関数、レシピ042 the_title_attribute関数 -->
        <?php the_title('<h2><a href="' . $permalink . '" title="' . the_title_attribute(array('before' => '投稿', 'after' => 'へのリンク', 'echo' => false)) . '">', '</a></h2>'); ?>
      <?php endif; ?>
      <div class="post-meta">
        <!-- レシピ046 the_date関数、レシピ048 get_month_link関数 -->
        <?php
          $year = get_the_date('Y');
          $month = get_the_date('m');
        ?>
        <p class="post-date">日付:<a href="<?php echo get_month_link($year, $month); ?>"><?php the_date(); ?></a></p>
        <!-- レシピ049 the_category関数 -->
        <div class="post-categories">カテゴリー:<?php the_category(); ?></div>
        <!-- レシピ050 the_tags関数 -->
        <div class="post-tags"><?php the_tags(); ?></div>
        <!-- レシピ045 the_author_posts_link関数 -->
        <div class="post-author">著者:<?php the_author_posts_link(); ?></div>
      </div>
      <div class="post-content">
        <!-- レシピ043 the_content関数 -->
        <?php the_content(); ?>
        <!-- レシピ044 the_excerpt関数
        <?php the_excerpt(); ?>
        -->
      </div>
      <?php if (is_singular()) : ?>
        <!-- レシピ052 wp_link_pages関数 -->
        <?php wp_link_pages(); ?>
        <?php comments_template(); ?>
      <?php endif; ?>
    </article>
  <?php endwhile; ?>
  <!-- レシピ053 previous_posts_link／next_posts_link関数
  <ul>
    <li><?php previous_posts_link('&lt; 前'); ?></li>
    <li><?php next_posts_link('次 &gt;'); ?></li>
  </ul>
  -->
  <!-- レシピ054 paginate_links関数 -->
  <div class="paginate-links"><?php echo paginate_links(); ?></div>
<?php else : ?>
  <p>投稿がありません。</p>
<?php endif; ?>
