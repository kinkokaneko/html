<?php get_header(); ?>

    <div class="container">
      <div class="main">
        <div class="row">
          <div class="col-md-9">
            <h2>メインメニュー</h2>
            <?php
              wp_nav_menu(array(
                'theme_location' => 'main-menu',
                'container' => 'nav',
                'container_class' => 'main-menu-wrap',
                'container_id' => 'main-menu-wrap',
                'menu_class' => 'main-menu',
                'menu_id' => 'main-menu'
              ));
            ?>
            <?php get_template_part('loop'); ?>
          </div>
          <div class="col-md-3">
            <?php get_sidebar(); ?>
          </div>
        </div>
      </div>

<?php get_footer(); ?>
