<?php get_header(); ?>

    <div class="container">
      <div class="main">
        <div class="row">
          <div class="col-md-9">
            <h2>最初の投稿とその他の投稿で処理を分ける例</h2>
	    <!-- レシピ055 -->
            <?php if ( have_posts ( ) ) : while ( have_posts ( ) ) : the_post ( ); ?>
              <?php the_title('<h3>','</h3>'); ?>
              <?php if ($wp_query->current_post == 0) : ?>
              <div><?php the_excerpt ( ); ?></div>
              <?php endif; ?>
              <?php if ($wp_query->current_post != $wp_query->post_count - 1) : ?>
                <hr />
              <?php endif; ?>
            <?php endwhile; endif; ?>
            <!-- ループを巻き戻す -->
            <?php rewind_posts(); ?>
            <h2>奇数件目と偶数件目で処理を分ける</h2>
            <?php if ( have_posts ( ) ) : while ( have_posts ( ) ) : the_post ( ); ?>
              <?php if ($wp_query->current_post % 2 == 0) : ?>
                <div class="odd">
              <?php else : ?>
                <div class="even">
              <?php endif; ?>
              <?php the_title('<h3>','</h3>'); ?>
              </div>
            <?php endwhile; endif; ?>

          </div>
          <div class="col-md-3">
            <?php get_sidebar(); ?>
          </div>
        </div>
      </div>

<?php get_footer(); ?>
