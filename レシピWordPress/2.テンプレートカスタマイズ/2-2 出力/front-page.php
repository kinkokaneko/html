<?php get_header(); ?>

    <div class="container">
      <div class="main">
        <div class="row">
          <div class="col-md-9">
            <table class="table">
	<!-- (レシピ034/レシピ035/レシピ036/レシピ037) -->
              <tr><td>このテーマのディレクトリのURL</td><td><?php echo get_stylesheet_directory_uri ( ); ?></td></tr>
              <tr><td>このテーマのディレクトリのパス</td><td><?php echo get_stylesheet_directory ( ); ?></td></tr>
              <tr><td>親テーマのディレクトリのURL</td><td><?php echo get_template_directory_uri ( ); ?></td></tr>
              <tr><td>親テーマのディレクトリのパス</td><td><?php echo get_template_directory ( ); ?></td></tr>
            </table>
            <p>
              テーマのディレクトリの画像を表示する例<br />
              <img src="<?php echo get_stylesheet_directory_uri ( ); ?>/images/sample.jpg" />
            </p>
            <p>
              テーマのディレクトリのファイルを読み込む例<br />
              <?php
                $fname = get_stylesheet_directory ( ) . '/sample.txt';
                readfile($fname);
              ?>
            </p>
            <?php get_template_part('loop'); ?>
          </div>
          <div class="col-md-3">
            <?php get_sidebar(); ?>
          </div>
        </div>
      </div>

<?php get_footer(); ?>
