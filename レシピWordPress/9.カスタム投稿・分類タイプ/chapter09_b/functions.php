<?php
/* テーマの設定 */
function add_theme_support_cb() {
/*  add_theme_support('custom-background', ・・・);
  add_theme_support('custom-header', ・・・);
  register_default_headers(・・・);
  add_theme_support('post-formats', ・・・);
  register_nav_menus(・・・);*/
  add_theme_support('title-tag');
}
add_action('after_setup_theme', 'add_theme_support_cb');
/* スタイルシートとJavaScriptの組み込み(レシピ197／198参照) */
function enqueue_cb() {
	wp_enqueue_script('jquery');
	wp_enqueue_style('bootstrap', get_stylesheet_directory_uri() . '/css/bootstrap.min.css');
	wp_enqueue_style('mystyle', get_stylesheet_directory_uri() . '/css/style.css', 'bootstrap');
}
add_action('wp_enqueue_scripts', 'enqueue_cb');

/* レシピ 150,151,157,158 */
function add_product_type() {
  $args = array(
    'label' => '製品',
    'labels' => array(
      'all_items' => '製品一覧',
      'add_new_item' => '新規製品を追加',
      'new_item' => '新規製品',
      'edit_item' => '製品を編集',
      'view_item' => '製品を表示',
      'search_items' => '製品を検索',
      'not_found' => '製品が見つかりませんでした',
    ),
    'public' => true,
    'menu_position' => 5,
    'supports' => array('title', 'editor', 'thumbnail', 'excerpt', 'custom-fields', 'revision'),
    'has_archive' => true,
  );
  register_post_type('product', $args);
  $args = array(
    'label' => 'ジャンル',
    'labels' => array(
      'all_items' => 'ジャンル一覧',
      'edit_item' => 'ジャンルを編集',
      'view_item' => 'ジャンルを表示',
      'update_item' => 'ジャンルを更新',
      'add_new_item' => '新規ジャンルを追加',
      'parent_item' => '親カテゴリー',
      'parent_item_colon' => '親カテゴリー:',
      'search_items' => 'ジャンルを検索'
    ),
    'public' => true,
    'hierarchical' => true
  );
  register_taxonomy('genre', 'product', $args);
}
add_action('init', 'add_product_type');
/* レシピ 150 */
function my_rewrite_flush() {
  add_product_type();
  flush_rewrite_rules();
}
add_action('after_switch_theme', 'my_rewrite_flush');
?>
