<?php get_header(); ?>

    <div class="container">
      <div class="main">
        <div class="row">
          <div class="col-md-9">
            <h2>分類</h2>
            <ul>
              <!-- レシピ162 -->
              <?php
                wp_list_categories(array(
                  'taxonomy' => 'genre',
                  'title_li' => ''
                ));
              ?>
            </ul>

            <h2>最近の製品</h2>
            <?php
              /* レシピ164 */
              $c_query = new WP_Query(array(
                'post_type' => 'product',
                'posts_per_page' => 10,
                'orderby' => 'date',
                'order' => 'DESC'
              ));
              if ($c_query->have_posts()) : 
            ?>
            <ul>
            <?php while ($c_query->have_posts()) : $c_query->the_post(); ?>
              <li><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>(<?php the_time('Y年m月d日'); ?>)</li>
            <?php endwhile; ?>
            </ul>
            <?php else : ?>
              <p>製品はありません。</p>
            <?php endif; ?>

            <h2>スラッグが「table」の分類に属する製品</h2>
            <?php
              /* レシピ165 */
              $c_query = new WP_Query(array(
                'post_type' => 'product',
                'tax_query' => array(
                	array(
                    'taxonomy' => 'genre',
                    'terms' => 'table',
                    'field' => 'slug'
                  )
                )
              ));
              if ($c_query->have_posts()) : 
            ?>
            <ul>
            <?php while ($c_query->have_posts()) : $c_query->the_post(); ?>
              <li><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>(<?php the_time('Y年m月d日'); ?>)</li>
            <?php endwhile; ?>
            </ul>
            <?php else : ?>
              <p>スラッグが「table」の分類に属する製品はありません。</p>
            <?php endif; ?>

            <h2>分類(製品が多い順)</h2>
            <ul>
            <?php
            /* レシピ165 */
            $terms = get_terms('genre', array(
              'orderby' => 'count',
              'order' => 'desc'
            ));
            foreach ($terms as $term) :
            ?>
              <li><a href="<?php echo get_term_link($term); ?>"><?php echo $term->name . '(' . $term->count . ')'; ?></a></li>
            <?php endforeach; ?>
            </ul>
          </div>
          <div class="col-md-3">
            <?php get_sidebar(); ?>
          </div>
        </div>
      </div>

<?php get_footer(); ?>
