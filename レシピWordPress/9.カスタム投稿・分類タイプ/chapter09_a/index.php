<?php get_header(); ?>

    <div class="container">
      <div class="main">
        <div class="row">
          <div class="col-md-9">

<!-- レシピ 155 -->
<?php if (post_type_exists('product')) : ?>
  <p>
    <!-- レシピ152 -->
    <a href="<?php echo home_url('/product/'); ?>">製品一覧</a>
  </p>
<?php else : ?>
  <p>製品のカスタム投稿タイプは登録されていません。</p>
<?php endif; ?>
            <?php get_template_part('loop'); ?>
          </div>
          <div class="col-md-3">
            <?php get_sidebar(); ?>
          </div>
        </div>
      </div>

<?php get_footer(); ?>
