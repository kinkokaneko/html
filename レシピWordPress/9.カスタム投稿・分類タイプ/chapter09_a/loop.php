<?php if ( have_posts() ) : ?>
  <?php while ( have_posts() ) : the_post(); ?>
    <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
      <?php if (is_singular()) : ?>
        <?php the_title('<h2>', '</h2>'); ?>
      <?php else : ?>
        <?php $permalink = get_permalink(); ?>
        <?php the_title('<h2><a href="' . $permalink . '">', '</a></h2>'); ?>
      <?php endif; ?>
      <div class="post-meta">
        <!-- レシピ 154 -->
        <?php if (is_singular('post')) : ?>
          <p class="post-date">日付:<?php the_date(); ?></p>
        <?php elseif (is_singular('product')) : ?>
          <p>
          <!-- レシピ 153 -->
          <?php
            $year = get_the_date('Y');
            $month = get_the_date('m');
          ?>
          <a href="<?php echo get_month_link($year, $month); ?>?post_type=product">日付:<?php the_date(); ?></a>
          </p>
        <?php else : ?>
          <p class="post-date">日付:<?php the_date(); ?></p>
        <?php endif; ?>
        <?php
          $p_id = get_the_ID();
          /* レシピ 154 */
          $p_type = get_post_type($p_id);
          if ($p_type == 'post') : 
        ?>
          <div class="post-categories">カテゴリー:<?php the_category(); ?></div>
          <div class="post-tags"><?php the_tags(); ?></div>
        <?php elseif ($p_type == 'product') : ?>
          <!-- レシピ 159 -->
          <?php the_terms(get_the_ID ( ), 'genre', '<div>分類', ':', '</div>'); ?>
          <!-- レシピ 160,161 -->
          <!-- 動作を見たい場合は、この行と「コメントここまで」の行のコメントを外してください
          <?php
          $terms = get_the_terms(get_the_ID(), 'genre');
          if ($terms) :
          ?>
          <ul>
            <?php for ($i = 0; $i < count($terms); $i++) : ?>
            <li><a href="<?php echo get_term_link($terms[$i]); ?>"><?php echo $terms[$i]->name; ?></a></li>
            <?php endfor; ?>
          </ul>
          <?php endif; ?>
          コメントここまで -->
          <p>
          <!-- レシピ 163 -->
          <?php if (has_term('rock', 'genre')) : ?>
            「rock」のタームに属する
          <?php else : ?>
            「rock」のタームに属さない
          <?php endif; ?>
          </p>
        <?php endif; ?>
      </div>
      <div class="post-content">
        <?php the_content(); ?>
      </div>
      <?php if (is_singular()) : ?>
        <?php comments_template(); ?>
      <?php endif; ?>
    </article>
  <?php endwhile; ?>
  <div class="paginate-links"><?php echo paginate_links(); ?></div>
<?php else : ?>
  <p>投稿がありません。</p>
<?php endif; ?>
