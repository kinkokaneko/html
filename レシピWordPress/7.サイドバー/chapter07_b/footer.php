      <div class="footer">
        <div class="row">
          <div class="col-md-4">
            <?php
              /* レシピ126 */
              if (is_active_sidebar('bottom_area')) {
                dynamic_sidebar('bottom_area');
              }
            ?>
          </div>
          <div class="col-md-4">
            <?php
              if (is_active_sidebar('bottom_area-2')) {
                dynamic_sidebar('bottom_area-2');
              }
            ?>
          </div>
          <div class="col-md-4">
            <?php
              if (is_active_sidebar('bottom_area-3')) {
                dynamic_sidebar('bottom_area-3');
              }
            ?>
          </div>
        </div>
      </div>
    </div><!-- /.container -->


    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <?php wp_footer(); ?>
  </body>
</html>
