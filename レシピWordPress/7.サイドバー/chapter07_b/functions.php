<?php
/* テーマの設定 */
function add_theme_support_cb() {
/*  add_theme_support('custom-background', ・・・);
  add_theme_support('custom-header', ・・・);
  register_default_headers(・・・);
  add_theme_support('post-formats', ・・・);
  register_nav_menus(・・・);*/
  add_theme_support('title-tag');
}
add_action('after_setup_theme', 'add_theme_support_cb');
/* スタイルシートとJavaScriptの組み込み(レシピ197／198参照) */
function enqueue_cb() {
	wp_enqueue_script('jquery');
	wp_enqueue_style('bootstrap', get_stylesheet_directory_uri() . '/css/bootstrap.min.css');
	wp_enqueue_style('mystyle', get_stylesheet_directory_uri() . '/css/style.css', 'bootstrap');
}
add_action('wp_enqueue_scripts', 'enqueue_cb');

/* ウィジェットエリアの登録（レシピ125、126）*/
function add_sidebar_cb () {
  register_sidebar(array(
    'id' => 'right_sidebar',
    'name' => '右サイドバー',
    'description' => '各ページの右に表示するサイドバーです',
  ));
  register_sidebars(3, array(
    'id' => 'bottom_area',
    'name' => '下ウィジェットエリア %d',
    'description' => '各ページの下に表示するウィジェットエリアです',
  ));
  register_widget('WP_Now_Widget');
}
add_action('widgets_init', 'add_sidebar_cb');

/* オリジナルのウィジェット（レシピ127、128） */
class WP_Now_Widget extends WP_Widget {
  function __construct() {
    parent::__construct(
      'now_widget', 
      '現在の日時',
      array( 'description' => '現在の日時を出力します', )
    );
  }
  public function widget($args, $instance ) {
    $date_format = !empty($instance['date_format']) ? $instance['date_format'] : get_option('date_format');
    $time_format = !empty($instance['time_format']) ? $instance['time_format'] : get_option('time_format');
    echo $args['before_widget'];
    echo date($date_format . ' ' . $time_format);
    echo $args['after_widget'];
  }
  public function form($instance) {
    $date_format = !empty($instance['date_format']) ? $instance['date_format'] : '';
    $time_format = !empty($instance['time_format']) ? $instance['time_format'] : '';
?>
<p>
<label for="<?php echo $this->get_field_id('date_format'); ?>">date format</label>
<input id="<?php echo $this->get_field_id('date_format'); ?>"
        name="<?php echo $this->get_field_name('date_format'); ?>"
        value="<?php echo esc_attr($date_format); ?>" />
</p>
<p>
<label for="<?php echo $this->get_field_id('time_format'); ?>">time format</label>
<input id="<?php echo $this->get_field_id('time_format'); ?>"
        name="<?php echo $this->get_field_name('time_format'); ?>"
        value="<?php echo esc_attr($time_format); ?>" />
</p>
<?php
  }
  public function update($new_instance, $old_instance) {
    $instance = array();
    $instance['date_format'] = !empty($new_instance['date_format']) ? $new_instance['date_format'] : '';
    $instance['time_format'] = !empty($new_instance['time_format']) ? $new_instance['time_format'] : '';
    return $instance;
  }
}
?>
