<div id="sidebar" role="complementary">
  <ul>
    <li>
      <!-- レシピ117 -->
      <?php get_search_form ( ); ?>
    </li>

    <li>
      <h2>月別アーカイブ</h2>
      <ul>
      <!-- レシピ118 -->
      <?php wp_get_archives(array('show_post_count' => 1)); ?>
      </ul>
    </li>

    <li>
      <h2>年別アーカイブ</h2>
      <!-- レシピ118 -->
      <select onchange="location.href = this.options[this.selectedIndex].value;">
      <?php wp_get_archives(array('type' => 'yearly', format => 'option')); ?>
      </select>
    </li>

    <li>
      <h2>最近の記事</h2>
      <ul>
      <!-- レシピ119 -->
      <?php wp_get_archives(array('type' => 'postbypost', 'limit' => 5)); ?>
      </ul>
    </li>

    <li>
      <h2>カテゴリー</h2>
      <ul>
      <!-- レシピ120 -->
      <?php wp_list_categories(array('title_li' => '', 'show_count' => 1)); ?>
      </ul>
    </li>

    <li>
      <h2>カテゴリー(ドロップダウン)</h2>
      <ul>
      <!-- レシピ121 -->
      <?php wp_dropdown_categories(array('show_count' => 1)); ?>
      <script type="text/javascript">
        (function () {
          var cat_sel = document.getElementById('cat');
          cat_sel.onchange = function () {
            var cat_id = cat_sel.options[cat_sel.selectedIndex].value;
            location.href = '<?php echo esc_url(home_url('/')); ?>?cat=' + cat_id;
          }
        })();
      </script>
      </ul>
    </li>

    <li>
      <h2>固定ページ</h2>
      <ul>
<!-- レシピ122 -->
<?php
if (is_page ()) {
  $cur_page = get_queried_object ( );
  wp_list_pages(array(
    'title_li' => '',
    'show_date' => 'modified',
    'child_of' => $cur_page->ID
  ));
}
else {
  wp_list_pages();
}
?>
      </ul>
    </li>

    <li>
      <h2>カレンダー</h2>
      <!-- レシピ123 -->
      <?php get_calendar ( ); ?>
    </li>

    <li>
      <h2>投稿者</h2>
      <!-- レシピ124 -->
      <ul>
      <?php
        wp_list_authors(array(
          'optioncount' => 1,
          'show_fullname' => 1,
        ));
      ?>
      </ul>
     </li>
  </ul>
</div>
