<?php
/**
 * @package WordPress
 * @subpackage Theme_Compat
 * @deprecated 3.0
 *
 * This file is here for Backwards compatibility with old themes and will be removed in a future version
 *
 */

// Do not delete these lines
	if (!empty($_SERVER['SCRIPT_FILENAME']) && 'comments.php' == basename($_SERVER['SCRIPT_FILENAME']))
		die ('Please do not load this page directly. Thanks!');

	if ( post_password_required() ) { ?>
		<p class="nocomments"><?php _e('This post is password protected. Enter the password to view comments.'); ?></p>
	<?php
		return;
	}
?>

<!-- You can start editing here. -->

<!-- レシピ106 -->
<?php if ( have_comments() ) : ?>
	<h3 id="comments">
		<?php
                        /* レシピ104 */
			if ( 1 == get_comments_number() ) {
				/* translators: %s: post title */
				printf( __( 'One response to %s' ),  '&#8220;' . get_the_title() . '&#8221;' );
			} else {
				/* translators: 1: number of comments, 2: post title */
				printf( _n( '%1$s response to %2$s', '%1$s responses to %2$s', get_comments_number() ),
					number_format_i18n( get_comments_number() ),  '&#8220;' . get_the_title() . '&#8221;' );
			}
		?>
	</h3>

	<div class="navigation">
                <!-- レシピ105 -->
		<div class="alignleft"><?php previous_comments_link() ?></div>
		<div class="alignright"><?php next_comments_link() ?></div>
	</div>

	<ol class="commentlist">
        <!-- レシピ103, 109 -->
	<?php wp_list_comments(array('callback' => 'my_comment'));?>
	</ol>

	<div class="navigation">
		<div class="alignleft"><?php previous_comments_link() ?></div>
		<div class="alignright"><?php next_comments_link() ?></div>
	</div>
 <?php else : // this is displayed if there are no comments so far ?>

	<?php if ( comments_open() ) : ?>
		<!-- If comments are open, but there are no comments. -->

	 <?php else : // comments are closed ?>
		<!-- If comments are closed. -->
		<p class="nocomments"><?php _e('Comments are closed.'); ?></p>

	<?php endif; ?>
<?php endif; ?>

<!-- レシピ107 -->
<?php if ( comments_open() ) : ?>

<div id="respond">
<!-- レシピ102 -->
<?php comment_form(); ?>
</div>

<?php endif; // if you delete this the sky will fall on your head ?>

<?php
/* レシピ109 */
function my_comment($comment, $args, $depth) {
  $GLOBALS['comment'] = $comment;
  extract($args, EXTR_SKIP);
?>
  <!-- レシピ114　-->
  <div class="<?php comment_class('mycomment'); ?>">
    <!-- レシピ111,112,113,115　-->
    <p><?php echo get_avatar($comment, 64); comment_author(); ?>からのコメント(日時:<?php comment_date(); ?>, タイプ:<?php echo get_comment_type(); ?>)</p>
    <!-- レシピ110　-->
    <p><?php comment_text(); ?>
  </div>
<?php } ?>