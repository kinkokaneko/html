<?php if ( have_posts() ) : ?>
  <?php while ( have_posts() ) : the_post(); ?>
    <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
      <?php if (is_singular()) : ?>
        <?php the_title('<h2>', '</h2>'); ?>
      <?php else : ?>
        <?php $permalink = get_permalink(); ?>
        <?php the_title('<h2><a href="' . $permalink . '">', '</a></h2>'); ?>
      <?php endif; ?>
      <div class="post-meta">
        <p class="post-date">日付:<?php the_date(); ?></p>
        <div class="post-categories">カテゴリー:<?php the_category(); ?></div>
        <div class="post-tags"><?php the_tags(); ?></div>
      </div>
      <?php /* レシピ080 */ ?>
      <?php if (has_post_thumbnail()) : ?>
        <div class="post-thumbnail"><?php the_post_thumbnail(array(100, 75), array('alt' => get_the_title())); ?></div>
      <?php endif; ?>
      <div class="post-content">
        <?php the_content(); ?>
      </div>
      <?php if (is_singular()) : ?>
        <?php /* レシピ086 */ ?>
        <h2>この投稿に割り当てたアイキャッチ画像</h2>
        <?php
          $a_id = get_post_thumbnail_id();
          if ($a_id) :
            $post_org = $GLOBALS['post'];
            $GLOBALS['post'] = get_post($a_id);
        ?>
          <?php /* レシピ087 */ ?>
          <p><?php the_attachment_link($a_id, true,false,true); ?></p>
        <?php
          $GLOBALS['post'] = $post_org;
          else : ?>
          <p>アイキャッチ画像がありません</p>
        <?php endif; ?>
        <?php /* レシピ084 */ ?>
        <h2>この投稿に割り当てた画像</h2>
        <?php
          $images = get_attached_media('image');
          $post_org = $GLOBALS['post'];
          foreach ($images as $image) :
            $post = $image;
            setup_postdata($post);
        ?>
          <p>the_attachment_linkの例 <?php the_attachment_link($post->ID, true,false,true); ?></p>
          <?php /* レシピ088 */ ?>
          <?php $a_src = wp_get_attachment_image_src($post->ID); ?>
          <p>wp_get_attachment_image_srcの例 <img src="<?php echo $a_src[0]; ?>" width="<?php echo $a_src[1]; ?>"
height="<?php echo $a_src[2]; ?>" /></p>
          <p>wp_get_attachment_metadataの例</p>
          <pre>
            <?php
              $a_meta = wp_get_attachment_metadata($post->id);
              print_r($a_meta);
            ?>
          </pre>
        <?php
          endforeach;
          $GLOBALS['post'] = $post_org;
        ?>
        <?php comments_template(); ?>
      <?php endif; ?>
    </article>
  <?php endwhile; ?>
  <div class="paginate-links"><?php echo paginate_links(); ?></div>
<?php else : ?>
  <p>投稿がありません。</p>
<?php endif; ?>
