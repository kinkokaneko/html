<?php
/* テーマの設定 */
function add_theme_support_cb() {
  /* レシピ077 */
  add_theme_support('post-thumbnails');
  /* レシピ078 */
  set_post_thumbnail_size(200, 150, array('left', 'top'));
}
add_action('after_setup_theme', 'add_theme_support_cb');
/* スタイルシートとJavaScriptの組み込み(レシピ197／198参照) */
function enqueue_cb() {
	wp_enqueue_script('jquery');
	wp_enqueue_style('bootstrap', get_stylesheet_directory_uri() . '/css/bootstrap.min.css');
	wp_enqueue_style('mystyle', get_stylesheet_directory_uri() . '/css/style.css', 'bootstrap');
}
add_action('wp_enqueue_scripts', 'enqueue_cb');
/* レシピ081 */
add_image_size('my_size', 800, 600, false);
?>
