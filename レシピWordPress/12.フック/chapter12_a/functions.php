<?php
/* レシピ195 */
function the_title_cb($title, $id) {
  return mb_convert_kana($title, 'rn');
}
add_filter('the_title', 'the_title_cb', 10, 2);

/* レシピ196 */
function save_post_cb($post_id, $post, $update) {
  $count = get_post_meta($post_id, '_save_count', true);
  if ($count == '') {
    $count = 0;
  }
  update_post_meta($post_id, '_save_count', $count + 1, $count);
}
add_action('save_post', 'save_post_cb', 10, 3);

/* レシピ197 */
function enqueue_style_cb() {
  wp_enqueue_style('jquery_ui', 'https://code.jquery.com/ui/1.11.4/jquery-ui.min.js');
  wp_enqueue_style('mystyle', get_stylesheet_directory_uri() . '/mystyle.css', array('jquery_ui'));
}
add_action('wp_enqueue_scripts', 'enqueue_style_cb');

/* レシピ198 */
function enqueue_script_cb() {
  wp_enqueue_script('jquery');
  wp_enqueue_script('myscript', get_stylesheet_directory_uri() . '/myscript.js', array('jquery'));
}
add_action('wp_enqueue_scripts', 'enqueue_script_cb');

/* レシピ199 */
function pre_get_posts_cb($query) {
  if (is_home() && $query->is_main_query()) {
    $query->set('category__not_in', 1);
  }
}
add_action('pre_get_posts', 'pre_get_posts_cb');

/* レシピ201 */
function my_theme_title_cb($title, $len) {
  if ($len > 20) {
    $title = mb_substr($title, 0, 20) . '...';
  }
  return $title;
}
add_filter('my_theme_title', 'my_theme_title_cb', 10, 2);

function add_theme_support_cb() {
/*  add_theme_support('custom-background', ・・・);
  add_theme_support('custom-header', ・・・);
  register_default_headers(・・・);
  add_theme_support('post-formats', ・・・);
  register_nav_menus(・・・);*/
  add_theme_support('title-tag');
}
add_action('after_setup_theme', 'add_theme_support_cb');
function enqueue_cb() {
	wp_enqueue_script('jquery');
	wp_enqueue_style('bootstrap', get_stylesheet_directory_uri() . '/css/bootstrap.min.css');
	wp_enqueue_style('mystyle', get_stylesheet_directory_uri() . '/css/style.css', 'bootstrap');
}
add_action('wp_enqueue_scripts', 'enqueue_cb');
?>
