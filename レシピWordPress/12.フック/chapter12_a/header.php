<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="<?php esc_attr(bloginfo('description')); ?>">
    <meta name="author" content="">
    <link rel="icon" href="favicon.ico">
    <?php wp_head(); ?>
  </head>

  <body>

    <nav class="navbar navbar-fixed-top navbar-dark bg-inverse">
      <ul class="nav navbar-nav">
        <li class="nav-item active">
          <a class="nav-link" href="<?php echo home_url('/'); ?>">Home</a>
        </li>
      </ul>
    </nav>

    <div class="container">
      <div class="site-header">
        <div class="row">
          <div class="col-md-12">
            <?php if (is_home()) : ?>
              <h1>
                <a href="<?php echo home_url('/'); ?>">
                <?php
                  $title = get_bloginfo('name');
                  $len = mb_strlen($title);
                  /* レシピ202 */
                  echo apply_filters('my_theme_title', $title, $len);
                ?>
                </a>
              </h1>
            <?php else : ?>
              <div class="site-title"><a href="<?php echo home_url('/'); ?>"><?php bloginfo('name') ?></a></div>
            <?php endif; ?>
            <p><?php bloginfo('description'); ?></p>
          </div>
        </div>
      </div>
    </div>