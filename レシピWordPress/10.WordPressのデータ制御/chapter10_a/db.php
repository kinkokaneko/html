<?php
/* レシピ167 */
require_once('../wp/wp-load.php');
if (isset($argv[0])) {
  $mode = isset($argv[1]) ? $argv[1] : '';
  $id = isset($argv[2]) ? $argv[2] : 0;
}
else {
  $mode = isset($_GET['mode']) ? $_GET['mode'] : '';
  $id = isset($_GET['id']) ? $_GET['id'] : 0;
}

if ($mode == 'insert_post') {
  /* レシピ168 */
  $post = wp_insert_post(array(
    'post_title' => '投稿テスト',
    'post_content' => "投稿テストです。",
    'post_name' => 'tp',
    'post_author' => 1,
    'post_status' => 'publish',
/* カテゴリやタグを指定する場合は、以下の2行を実際の実際のカテゴリやタグに合わせて書き換えてから、コメントを外してください。
    'post_category' => array(10, 20),
    'tags_input' => array('タグ1', 'タグ2')
コメントここまで */
  ));
  if ($post) {
    echo "IDが{$post}番の投稿を作成しました。";
  }
  else {
    echo "投稿の作成に失敗しました。";
  }
}
elseif ($mode == 'update_post' && $id) {
  /* レシピ169 */
  $post = wp_insert_post(array(
    'ID' => $id,
    'post_title' => '投稿更新テスト',
    'post_content' => '投稿更新テストです。',
  ));
  if ($post) {
    echo "IDが{$post}番の投稿を更新しました。";
  }
  else {
    echo "投稿の更新に失敗しました。";
  }
}
elseif ($mode == 'insert_cat') {
  /* レシピ170 */
  require_once('../wp/wp-admin/includes/taxonomy.php');
  $cat = wp_insert_category(array(
    'cat_name' => 'カテゴリーテスト',
    'category_description' => 'カテゴリーテストです。',
    'category_nicename' => 'cattest',
    /* 以下の行の「1」を実際の親カテゴリーのIDに変えてからコメントを外してください。
    'category_parent' => 1
    コメントここまで */
  ));
  if ($cat) {
    echo "IDが{$cat}番の投カテゴリーを作成しました。";
  }
  else {
    echo "カテゴリーの作成に失敗しました。";
  }
}
elseif ($mode == 'insert_attachment') {
  /* レシピ171 */
  /* 「WordPressのパス」の部分は実際の環境に合わせて書き換えてください。 */
  require_once('../wp/wp-admin/includes/image.php' );
  $filename = '../wp/wp-content/uploads/2016/01/sample.png';
  $upload_dir = wp_upload_dir();
  $aid = wp_insert_attachment(array(
    'guid' => $upload_dir['url'] . '/' . basename($filename),
    'post_mime_type' => 'image/png',
    'post_title' => 'sample',
    'post_content' => '',
    'post_status' => 'inherit',
    'post_author' => 1,
  ), $filename);
  $attach_data = wp_generate_attachment_metadata($aid, $filename);
  wp_update_attachment_metadata($aid, $attach_data);
  if ($aid) {
    echo "IDが{$aid}番のメディアを作成しました。";
  }
  else {
    echo 'メディアの作成に失敗しました。';
  }
}
elseif ($mode == 'set_terms' && $id) {
  /* レシピ172 */
  /* genreカスタム分類に、「genre1」と「genre2」のタームが存在していることが必要です。 */
  $ret = wp_set_object_terms($id, array('genre1', 'genre2'), 'genre');
  if ($ret) {
    echo 'タームを設定しました。';
  }
  else {
    echo 'タームの設定に失敗しました。';
  }
}
elseif ($mode == 'add_meta' && $id) {
  /* レシピ173 */
  $m_id = add_post_meta($id, 'foo', 'bar');
  if ($m_id) {
    echo 'fooカスタムフィールドを追加しました。';
  }
  else {
    echo 'fooカスタムフィールドの追加に失敗しました。';
  }
}
elseif ($mode == 'update_meta' && $id) {
  /* レシピ174 */
  $m_id = update_post_meta($id, 'foo', 'baz', 'bar');
  if ($m_id) {
    echo 'fooカスタムフィールドを更新しました。';
  }
  else {
    echo 'fooカスタムフィールドの更新に失敗しました。';
  }
}
elseif ($mode == 'delete_post' && $id) {
  /* レシピ175 */
  $p_id = wp_delete_post($id);
  if ($p_id) {
    echo "IDが{$id}の投稿を削除しました。";
  }
  else {
    echo '投稿の削除に失敗しました。';
  }
}
elseif ($mode == 'trash_post' && $id) {
  /* レシピ175 */
  $p_id = wp_trash_post($id);
  if ($p_id) {
    echo "IDが{$id}の投稿をゴミ箱に移動しました。";
  }
  else {
    echo '投稿をゴミ箱に移動するのに失敗しました。';
  }
}
elseif ($mode == 'delete_attachment' && $id) {
  /* レシピ175 */
  $a_id = wp_delete_attachment($id);
  if ($a_id) {
    echo "IDが{$id}のメディアを削除しました。";
  }
  else {
    echo 'メディアを削除するのに失敗しました。';
  }
}
elseif ($mode == 'delete_cat' && $id) {
  /* レシピ175 */
  $c_id = wp_delete_term($id, 'category');
  if ($c_id) {
    echo "IDが{$id}のカテゴリーを削除しました。";
  }
  else {
    echo 'カテゴリーを削除するのに失敗しました。';
  }
}
elseif ($mode == 'get_option') {
  /* レシピ175 */
  $opt = get_option('date_format');
  echo "date_formatの値は {$opt} です。";
}
elseif ($mode == 'update_option') {
  update_option('date_format', 'Y/m/d');
  echo "date_formatの値を Y/m/d に変更しました。";
}
else {
  echo "パラメータが正しくありません。 mode = $mode, id = $id";
}
?>

