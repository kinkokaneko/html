<?php get_header(); ?>

    <div class="container">
      <div class="main">
        <div class="row">
          <div class="col-md-9">
<?php
/* レシピ178 */
/* なお、通常のテンプレート内では、このglobal文がなくても動作します */
global $wpdb;

/* レシピ184 */
/* この後のサンプルに含まれるSQLに意図的に構文ミスの箇所を入れると、エラーメッセージの表示を試すことができます。 */
$wpdb->show_errors ();
?>

            <h2>get_resultsメソッドの例(最近の未公開の投稿5件)</h2>
            <ul>
            <?php
              /* レシピ179 */
              $sql = <<<HERE
              SELECT * FROM $wpdb->posts
              WHERE post_author = 1
              AND post_type = 'post'
              AND post_status = 'draft'
              ORDER BY post_date
              LIMIT 5
HERE;
              $results = $wpdb->get_results($sql);
              global $post;
              foreach ($results as $post) :
                setup_postdata($post);
            ?>
              <li><?php the_title(); ?>(<?php the_author(); ?>, <?php the_time('Y/m/d'); ?>)</li>
            <?php endforeach; ?>
            </ul>

            <h2>get_rowメソッド(最新の未公開投稿)</h2>
            <?php
              /* レシピ180 */
              $sql = <<<HERE
              SELECT * FROM $wpdb->posts
              WHERE post_author = 1
              AND post_type = 'post'
              AND post_status = 'draft'
              ORDER BY post_date
              LIMIT 1
HERE;
              $post = $wpdb->get_row($sql);
              setup_postdata($post);
            ?>
            <p><?php the_title(); ?>(<?php the_author(); ?>, <?php the_time('Y/m/d'); ?>)</p>

            <h2>get_varメソッド(IDが1番のユーザーの公開済みの投稿数)</h2>
            <p>
            <?php
              /* レシピ181*/
              $sql = <<<HERE
              SELECT COUNT(*) FROM $wpdb->posts
              WHERE post_author = 1
              AND post_type = 'post'
              AND post_status = 'publish'
HERE;
              $count = $wpdb->get_var($sql);
              echo $count;
            ?>
            </p>

            <h2>prepareメソッド(特定のユーザーの投稿)</h2>
            <ul>
            <?php
              /* レシピ183 */
              /* 変数user_idとpost_countの値を変えてお試しください。 */
              $user_id = 2;
              $post_count = 10;
              $sql = <<<HERE
              SELECT * FROM $wpdb->posts
              WHERE post_author = %d
              AND post_type = 'post'
              AND post_status = 'publish'
              ORDER BY post_date
              LIMIT %d
HERE;
              $sql = $wpdb->prepare($sql, array($user_id, $post_count));
              $posts = $wpdb->get_results($sql);
              global $post;
              foreach ($posts as $post) :
                setup_postdata($post);
            ?>
              <li><?php the_title(); ?>(<?php the_author(); ?>, <?php the_time('Y/m/d'); ?>)</li>
            <?php endforeach; ?>
            </ul>
          </div>
          <div class="col-md-3">
            <?php get_sidebar(); ?>
          </div>
        </div>
      </div>

<?php
/* レシピ184 */
$wpdb->hide_errors ();
?>

<?php get_footer(); ?>
