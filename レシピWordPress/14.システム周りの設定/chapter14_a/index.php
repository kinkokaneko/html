<?php get_header(); ?>

    <div class="container">
      <div class="main">
        <div class="row">
          <div class="col-md-9">
            <h1>各サイトの最新の投稿（サイトごとにまとめたもの）</h1>
            <!-- レシピ223 -->
            <?php
            $sites = wp_get_sites();
            foreach ($sites as $site) :
                switch_to_blog($site['blog_id']);
            ?>
            <h2><a href="<?php echo esc_url(home_url('/')); ?>"><?php bloginfo('name'); ?></a></h2>
            <?php
                $s_query = new WP_Query(array(
                    'posts_per_page' => 5
                ));
                if ($s_query->have_posts()) :
            ?>
            <ul>
                <?php while ($s_query->have_posts()) : $s_query->the_post(); ?>
                	<li><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></li>
                <?php endwhile; ?>
            </ul>
            <?php
                endif;
                restore_current_blog();
            endforeach;
            ?>

            <h1>各サイトの最新の投稿（サイトを混在）</h1>
            <!-- レシピ224 -->
            <ul>
            <?php
            global $wpdb, $post;
            $sql = <<<HERE
            SELECT * FROM {$wpdb->prefix}allposts
            WHERE post_type = 'post'
            AND post_status = 'publish'
            ORDER BY post_date DESC
            LIMIT 10
HERE;
            $allposts = $wpdb->get_results($sql);
            foreach ($allposts as $post) {
              setup_postdata($post);
              switch_to_blog($post->post_blog_id);
            ?>
              <li>
                <a href="<?php the_permalink(); ?>"><?php the_title(); ?>
                (<a href="<?php echo home_url(); ?>"><?php bloginfo('name'); ?></a>)
              </li>
            <?php
              restore_current_blog();
            }
            ?>
            </ul>
          </div>
          <div class="col-md-3">
            <?php get_sidebar(); ?>
          </div>
        </div>
      </div>

<?php get_footer(); ?>
