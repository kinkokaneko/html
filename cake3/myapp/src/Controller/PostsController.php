<?php
/**
 * Created by PhpStorm.
 * User: kinko
 * Date: 2019-03-31
 * Time: 00:02
 */

// /posts/index

namespace App\Controller;

class PostsController extends AppController{
    public function index(){
        $posts=$this->Posts->find('all');
        $this->set('posts',$posts);
    }
}
