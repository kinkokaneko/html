<?php
$this->assign('title','Add New');?>

<h1>
    <?= $this->Html->link('Back', ['action'=>'index'], ['class'=>['pull-right', 'fs12']]); ?>
    <?= h($post->title); ?>

Add New</h1>

<?= $this->Form->create(null,[
    'url'=>['controller'=>'Comments','action'=>'add']
]); ?>
<?= $this->Form->input('body'); ?>
<?= $this->Form->hidden('post_id',['value'=>$post->id]); ?>
<?= $this->Form->button('Add'); ?>
<?= $this->Form->end(); ?>




