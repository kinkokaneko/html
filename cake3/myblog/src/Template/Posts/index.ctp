<h1>
    <?= $this->Html->link('Add New', ['action'=>'add'], ['class'=>['pull-right', 'fs12']]); ?>

    Blog Posts</h1>

<ul>
    <?php foreach ($posts as $post) : ?>
        <li>
<!--            Htmlヘルパーの場合-->
            <?= $this->Html->link($post->title,['controller'=>'Posts','action'=>'view',$post->id]); ?>
            <?= $this->Html->link('[Edit]',['action'=>'edit',$post->id]); ?>
<!--            Urlヘルパーの場合-->
            <a href="<?= $this->Url->build(['action'=>'view',$post->id],['class'=>'fs12']); ?>">　
                <?= h($post->title); ?>
                <?=
                $this->Form->postLink(
                    '[x]',
                    ['action'=>'delete',$post->id],
                    ['confirm'=>'Are you sure','class'=>'fs12']
                );


                ?>


            </a>
        </li>
    <?php endforeach; ?>
</ul>
