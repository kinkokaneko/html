<?php
$this->assign('title','Blog Detail');?>

<h1>
    <?= $this->Html->link('Back', ['action'=>'index'], ['class'=>['pull-right', 'fs12']]); ?>
    <?= h($post->title); ?></h1>
<P>

    <?= nl2br(h($post->body)); ?>
<h2>Comments<span class="fs12">(<?= count($post->comments); ?></span></h2>
<h2>New Comments</h2>
    <?= $this->Form->create($post); ?>
    <?= $this->Form->input('title'); ?>
    <?= $this->Form->input('body',['rows'=>'3']); ?>
    <?= $this->Form->button('Add'); ?>
    <?= $this->Form->end(); ?>
</P>

