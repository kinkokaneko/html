<?php
/**
 * Created by PhpStorm.
 * User: kinko
 * Date: 2019-03-31
 * Time: 12:35
 */


namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\Validation\Validator;

class CommentsTable extends Table{
    public function initialize(array $config){
        $this->addBehavior('Timestamp');
        $this->belongsTo('Posts');
    }
    public function validationDefault( Validator $validator ) {

        $validator

            ->notEmpty('body')
            ->requirePresence('body');
        return $validator;
    }


}

