<?php
/**
 * Created by PhpStorm.
 * User: kinko
 * Date: 2019-03-31
 * Time: 12:48
 */

namespace App\Controller;

class PostsController extends AppController{

    public function index(){

        $this->viewBuilder()->layout('my_layout');
        $posts=$this->Posts->find('all');
//            ->order(['title'=>'DESC'])
//            ->limit(2)
//            ->where(['title like'=>'%3']);
        $this->set('posts',$posts);
    }

    public function view($id=null){

        // $post = $this->Posts->get($id);
        $post = $this->Posts->get($id, [
            'contain' => 'Comments'
        ]);
        $this->set(compact('post'));
    }
    public function add(){

        $post = $this->Posts->newEntity();
        if ($this->request->is('post')) {

            $post = $this->Posts->patchEntity($post, $this->request->data);
           if($this->Posts->save($post)){
               $post->Flash->success('Add Sccess!');
               return $this->redirect(['action'=>'index']);

           }else{
           $this->Flash->error('Add Error!');

           }

              }
        $this->set(compact('post'));
    }

    public function edit($id=null){

        $post = $this->Posts->get($id);
        if ($this->request->is(['post','patch','put'])) {

            $post = $this->Posts->patchEntity($post, $this->request->data);
            if($this->Posts->save($post)){
                $post->Flash->success('Edit Sccess!');
                return $this->redirect(['action'=>'index']);

            }else{
                $this->Flash->error('Edit Error!');

            }

        }
        $this->set(compact('post'));
    }


    public function delete($id=null){

        $this->request->allowMethod(['post','delete']);

        $post = $this->Posts->get($id);

            if($this->Posts->delete($post)){
                $post->Flash->success('delete Sccess!');


            }else{
                $this->Flash->error('Delete Error!');

            }
        return $this->redirect(['action'=>'index']);

        }


}
