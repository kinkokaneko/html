// 

//non blocking 次の処理をブロックしない書き方 node.jsではよく使う
//コールバック関数（タイマー処理が終わったあとに実行する処理を関数で渡す）
//この書き方をすることでsetTimeout終わる前に次の出力書処理が行われる
setTimeout(function(){
    console.log("hello");
},1000);
console.log("world")


//blocking
var start=new Date().getTime;
while(new Date().getTime()<start+1000);
console.log("world");

//時間がかかりそうな処理はコールバック関数で実装する