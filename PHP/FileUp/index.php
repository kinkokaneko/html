<?php

ini_set('display_errors',1);
define('IMAGES_DIR',__DIR__.'/images');
define('THUMBNAIL_DIR',__DIR__.'/thumbs');

function h($s){
    return htmlspecialchars($s,ENT_QUOTES,"UTF-8");
}

require "ImageUploader.php";

$uploader=new\FileUp\ImageUploader();

if($_SERVER['REQUEST_METHOD']==='POST'){
    $uploader->upload();
}


?>





<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>ファイルアップロードのテスト</title>
</head>

<body>
<p>ファイルアップロード</p>
<form type="file" method="post" action="index.php" enctype="multipart/form-data">
    <input type="hidden" name="MAX_FILE_SIZE" value="1000000">
    画像<input type="file" name="upload"><br>
    説明<input type="text" name="comment"><br>
    <input type="submit" value="ファイルアップロード">
</form>
</body>

</html>