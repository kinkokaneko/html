<?php
/**
 * WordPress の基本設定
 *
 * このファイルは、インストール時に wp-config.php 作成ウィザードが利用します。
 * ウィザードを介さずにこのファイルを "wp-config.php" という名前でコピーして
 * 直接編集して値を入力してもかまいません。
 *
 * このファイルは、以下の設定を含みます。
 *
 * * MySQL 設定
 * * 秘密鍵
 * * データベーステーブル接頭辞
 * * ABSPATH
 *
 * @link http://wpdocs.osdn.jp/wp-config.php_%E3%81%AE%E7%B7%A8%E9%9B%86
 *
 * @package WordPress
 */

// 注意:
// Windowy
//s の "メモ帳" でこのファイルを編集しないでください !
// 問題なく使えるテキストエディタ
// (http://wpdocs.osdn.jp/%E7%94%A8%E8%AA%9E%E9%9B%86#.E3.83.86.E3.82.AD.E3.82.B9.E3.83.88.E3.82.A8.E3.83.87.E3.82.A3.E3.82.BF 参照)
// を使用し、必ず UTF-8 の BOM なし (UTF-8N) で保存してください。

// ** MySQL 設定 - この情報はホスティング先から入手してください。 ** //
/** WordPress のためのデータベース名 */
define('DB_NAME', 'userdb');

/** MySQL データベースのユーザー名 */
define('DB_USER', 'kaneko');

/** MySQL データベースのパスワード */
define('DB_PASSWORD', 'mdhiraoka1021');

/** MySQL のホスト名 */
define('DB_HOST', 'localhost');

/** データベースのテーブルを作成する際のデータベースの文字セット */
define('DB_CHARSET', 'utf8mb4');

/** データベースの照合順序 (ほとんどの場合変更する必要はありません) */
define('DB_COLLATE', '');

/**#@+
 * 認証用ユニークキー
 *
 * それぞれを異なるユニーク (一意) な文字列に変更してください。
 * {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org の秘密鍵サービス} で自動生成することもできます。
 * 後でいつでも変更して、既存のすべての cookie を無効にできます。これにより、すべてのユーザーを強制的に再ログインさせることになります。
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '(_QnzjHw(AkPxsh7}Ra2WGgl!/he7y5kuHK:Csf7EvI;DW$2NGsvzx?j$~eg=R}<');
define('SECURE_AUTH_KEY',  'mzy[N{q&|^@01+Z!tUb4L+1.Grmf~k0sQ/[-@<#j=(SdA9#6#rA@4s%NtWmpUH4p');
define('LOGGED_IN_KEY',    'wYTQ;RdJ})2:5g{2d.)$^]y,E)Wrc/5>PSk&x|C.}F=87mWL8ViUe Ev[/qV}|y9');
define('NONCE_KEY',        '1+PyJI6bbPj.{>,CN&u3;iFriK_^4yq<z}4SM-F.Ju[~k~HTtBxL}|0w4lYg=B8`');
define('AUTH_SALT',        '>>xC`Wk`A 55@o7o~P[(!t|FfeR$7p8I<d4aG_?i; dcYd!W8,O)0eTz)>#_4v[R');
define('SECURE_AUTH_SALT', 'Y^#h?MZqV1l7b,3S*mqZF;K t=f<z8(I|5wc@Y^/oTL0Wp8G!,QASH]DW:iGD~|Y');
define('LOGGED_IN_SALT',   'oD}xHN#5(B2OjrPf~JCL&0$>(5#CB87vg$6-)CTzry*<[$0zHea/1,Yy]LX;!+`k');
define('NONCE_SALT',       'x(o8,G9)CyV6@q*9|[*+j:Cwz@<m>!}[nCZ@!(5nR/1D=z1zWRXB&DwpADQ_u>|;');

/**#@-*/

/**
 * WordPress データベーステーブルの接頭辞
 *
 * それぞれにユニーク (一意) な接頭辞を与えることで一つのデータベースに複数の WordPress を
 * インストールすることができます。半角英数字と下線のみを使用してください。
 */
$table_prefix  = 'wp_';

/**
 * 開発者へ: WordPress デバッグモード
 *
 * この値を true にすると、開発中に注意 (notice) を表示します。
 * テーマおよびプラグインの開発者には、その開発環境においてこの WP_DEBUG を使用することを強く推奨します。
 *
 * その他のデバッグに利用できる定数については Codex をご覧ください。
 *
 * @link http://wpdocs.osdn.jp/WordPress%E3%81%A7%E3%81%AE%E3%83%87%E3%83%90%E3%83%83%E3%82%B0
 */
define('WP_DEBUG', false);

/* 編集が必要なのはここまでです ! WordPress でブログをお楽しみください。 */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
