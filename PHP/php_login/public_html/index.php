<?php

//ユーザーの一覧

require_once (__DIR__.'/../config/config.php');

//var_dump($_SESSION['me']);
$app=new MyApp\Controller\index();

$app->run();

//$app->me() ログインしているユーザーの情報取得
//$app->getValues()->users 登録されているユーザーの取得
?>


<!DOCTYPE html>
<html lang="ja">
<head>
	<meta charset="utf-8">
	<title>Home</title>
	<link rel="stylesheet" href="styles.css">
</head>
<body>
<div id="container">
	<form action="logout.php" method="post" id="logout">
		<?php echo $app->me()->email;?><input type="submit" name="token" value="LogOut">

	</form>
	<h1>users<span class="fs12"><?php echo count($app->getValues()->users);?></span></h1>
	<ul>
		<?php foreach ($app->getValues()->users  as $user): ?>

		<li><?= h($user->email);?></li>
		<?php endforeach;?>


	</ul>

</div>
</body>
</html>
