<?php

namespace MyApp\Exception;

class UnmatchEmailOrPassword extends \Exception {
	protected $message = 'email/passwords do not match!';
}