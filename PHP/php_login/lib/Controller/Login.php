<?php

namespace MyApp\Controller;

class Login extends \MyApp\Controller {

	public function run() {
		if ($this->isLoggedIn()) {
			header('Location: ' . SITE_URL);
			exit;
		}

		if ($_SERVER['REQUEST_METHOD'] === 'POST') {
			$this->postProcess();
		}
	}

	protected function postProcess() {
		    // validate
		try {
			$this->_validate();
		} catch (\MyApp\Exsception\EmptyPost $e) {
			$this->setErrors('login', $e->getMessage());
		}

		$this->setValues('email', $_POST['email']);

		if ($this->hasError()) {
			return;

		} else {


			// create user
			try {
				$userModel = new \MyApp\Model\User();
				$user=$userModel->login([
					'email' => $_POST['email'],
					'password' => $_POST['password']
				]);
			} catch (\MyApp\Exception\UnmatchEmailOrPassword $e) {
				$this->setErrors('login', $e->getMessage());
				return;
			}

			//login
			//セッションハイジャック対策
			session_regenerate_id(true);


			$_SESSION['me']=$user;


			// redirect to home
			header('Location: ' . SITE_URL );
			exit;
		}
	}

	private function _validate() {
//		if (!isset($_POST['token']) || $_POST['token'] !== $_SESSION['token']) {
//			echo "Invalid Token!";
//			exit;
//		}

		if (!isset($_POST['token']) || !isset($_POST['password'])){
			echo "Invalid Form!";
			exit;
		}


		if ($_POST['token']===''||$_POST['password']==='') {
			throw new \MyApp\Exception\EmptyPost();
		}
	}

}