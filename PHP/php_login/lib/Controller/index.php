<?php
/**
 * Created by PhpStorm.
 * User: kinko
 * Date: 2019-04-27
 * Time: 23:35
 */

namespace MyApp\Controller;

use phpDocumentor\Reflection\Types\This;

class Index extends \MyApp\Controller{

	public function run(){

		if(!$this->isLoggedIn()){
			//login
			header('Location:'.SITE_URL.'/login.php');
			exit;
		}


	//ge users info

	$userModel= new \MyApp\Model\User();
	$this->setValues('users',$userModel->findAll());
}
}