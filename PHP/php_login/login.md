# ログイン処理実装手順

## フォルダ構成
MVCデザインパターンに沿ってフォルダ構成を整える
- フォルダ構成
  - congfig(設定)
  - lib(Controller、Model)
  - public_html(View)
  - function.php

```
root/
 ┣ config
   └autoload.php
   └config.php

 ┣ lib
      ┣ Controller
          └ index.php
          └ signout.php
          └ login.php

      ┣ Exception
          └ DuplicateEmail.php
          └ EmptyPost.php
          └ InvalidEmail.php
          └ InvalidPassword.php
          └ UnmatchEmailOrPassword.php

      ┣ Model
          └ User.php

   └funciton.php
   └Controller.php
   └Model.php

 ┣ public_html
   └index.php
   └signup.php
   └login.php
   └logout.php

  

```



## DB設定
- MySQLの設定
- SQLの実行
- 動作確認

## アプリの設定
- config.php
  - DB設定
  - display_error
  - セッション設定

- autoload.php
  - クラスのオートロード設定

## IndexのController作成
  - index.php(View)
  - Index.php(Contoroller)

## ログイン画面の作成
  - ログインの判定
  - login.php(View)
  - Login.php(Controller)
  - login.phpのHTML作成

## 新規登録画面の作成
  - signup.php(View)
  - Signup.php(Controller)

## 投稿時のValidation設定
・postProcess()
・_validate()
・InvalidEmail InvalidPasssword
・エラー情報の保持
・エラー情報の取得、表示

## $_errorsオブジェクトの作成
 - setErrors() hasErrors() getErrors()

## $_valuesオブジェクトの作成
 - setValues()
 - getValues()

## CSRF対策

## ユーザー新規登録
 - 新規登録
 - ログイン画面へのリダイレクト
 - DUpilicateEmail.php(Exception)

## ユーザー情報格納
 - create()
 - 動作確認

## LoginController作成
 - login.php(Controller)

## LoginのView作成
 - login.php(View)
 - ログイン処理
 - セッションハイジャック対策

## ログイン処理実装
 - login()
 - 動作確認

## IndexのView作成
 - index.php

## ログアウト処理を実装
 - logout.php
 - 動作確認

## Indexにデータ埋め込み

## ユーザーの一覧を取得
 - me()
 - findAll()
 - 動作確認


