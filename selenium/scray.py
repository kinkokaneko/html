import urllib3
from bs4 import BeautifulSoup
import certifi
import re

# アクセスするURL
url = "https://gihyo.jp/book/2017/978-4-7741-8780-8#toc"

# httpsの証明書検証を実行している
http = urllib3.PoolManager(
    cert_reqs='CERT_REQUIRED',
    ca_certs=certifi.where())
r = http.request('GET', url)

# htmlをBeautifulSoupで扱う
soup = BeautifulSoup(r.data, "html.parser")

# h3要素の文字列を取得 → Chapter 1　HTML／CSSの基礎
for RES in soup.find_all("h3"):
    title = RES.string

    #Chapterを含むh3文字列
    if("Chapter" in title):
      print(title)

com=re.compile("\d{3}")

for RES in soup.find_all("li"):
    title = RES.string

    #数字三桁を含むli文字列
    if com.match(title):
      print(title)
