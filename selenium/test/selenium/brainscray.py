from urllib3 import PoolManager
from bs4 import BeautifulSoup
import requests


# アクセスするURL
url = requests.get('http://52.199.55.80/project/search?salary_kbn=&salary=&feature_major=1', auth=('brainnet', 'p_8J4=R!ML1R'))

# httpsの証明書検証を実行している
http = PoolManager()
r = http.request('GET', url)

# htmlをBeautifulSoupで扱う
soup = BeautifulSoup(r.data, "html.parser")

#文字列を取得
for RES in soup.find_all("h2"):
    print(RES.string)

