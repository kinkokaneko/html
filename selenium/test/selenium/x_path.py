
import time
class x_path:



  def start(get_xpath,CHECKIN_XPATH,SEARCH,ACORDION,FILENAME,CHECKOUT_XPATH,self):
   # エリアにチェック入れる
   check_box_in = get_xpath(CHECKIN_XPATH)
   check_box_in.click()

   # 一時停止
   time.sleep(2)

   # 検索ボタン
   search_btn = get_xpath(SEARCH)
   search_btn.click()

   # 一時停止
   time.sleep(2)

   # アコーディオン開く
   company_open = get_xpath(ACORDION)
   company_open.click()

   # 一時停止
   time.sleep(2)

   # スクショ
   self.browser.get_screenshot_as_file(FILENAME)

   # 一時停止
   time.sleep(2)

   # チェック外す
   check_box_out = get_xpath(CHECKOUT_XPATH)
   check_box_out.click()

   # 一時停止
   time.sleep(2)
