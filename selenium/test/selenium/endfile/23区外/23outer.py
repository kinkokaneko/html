import unittest
from selenium import webdriver
import time
import os


class SampleBrainTest(unittest.TestCase):

    def setUp(self):
        # ブラウザ起動
        self.browser = webdriver.Chrome(
            '/Users/t.kaneko/works/kanekoprac/selenium/test/selenium/chromedriver')
        # 5秒停止
        time.sleep(3)
        # 画面最大化
        self.browser.maximize_window()

    def tearDown(self):
        self.browser.quit()


    def test_checkbox_workLocation(self):

        # URL
        URL = "http://brainnet:p_8J4=R!ML1R@52.199.55.80/project/search?area_ids%5B%5D=2&spot_ids%5B%5D=8&salary_kbn=&salary="

        SEARCH = "/html/body/div[1]/main/div[2]/aside/form/div/button"
        ACORDION = "/html/body/div[1]/main/div[2]/aside/form/section[2]/div/ul/li[1]/div[1]"

        sleeptime=1

        #path取得
        get_xpath=self.browser.find_element_by_xpath

        # サイト開く
        self.browser.get(URL)

        # アコーディオン開く
        company_open = get_xpath(ACORDION)
        company_open.click()

        time.sleep(sleeptime)

        #検索
        for i in range(1, 7):
                # 取得したい要素のXPATH
                CHECKOUT_XPATH="/html/body/div[1]/main/div[2]/aside/form/section[2]/div/ul/li[1]/div[2]/div/ul/li["+str(i)+"]/label/span"
                CHECKIN_XPATH="/html/body/div[1]/main/div[2]/aside/form/section[2]/div/ul/li[1]/div[2]/div/ul/li["+str(i)+"]/label/span"

                # スクショ保存パス
                FILENAME = os.path.join(os.path.dirname(os.path.abspath(__file__)), "screen"+str(i)+".png")

                # エリアにチェック入れる
                check_box_in = get_xpath(CHECKIN_XPATH)
                check_box_in.click()

                # 一時停止
                time.sleep(sleeptime)

                # 検索ボタン
                search_btn = get_xpath(SEARCH)
                search_btn.click()

                # 一時停止
                time.sleep(sleeptime)

                # アコーディオン開く
                company_open = get_xpath(ACORDION)
                company_open.click()


                # 一時停止
                time.sleep(sleeptime)

                # スクショ
                self.browser.get_screenshot_as_file(FILENAME)

                # 一時停止
                time.sleep(sleeptime)

                # チェック外す
                check_box_out = get_xpath(CHECKOUT_XPATH)
                check_box_out.click()

                # 一時停止
                time.sleep(sleeptime)




if __name__ == '__main__':
    unittest.main(verbosity=2)
