<?php
session_start();
?>
<?php require 'master_api.php'; ?>
<?php include '../ja/dom/air_info/cargo/functions/functions.php'; ?>
<?php

if($_POST){
	if (isset($_POST['capacity'])) {
		$jp = ['年','月','日'];
		$_POST['weight'] = preg_replace("/[^0-9]/","",$_POST['weight']);
		$_POST['length'] = preg_replace("/[^0-9]/","",$_POST['capacity']['height']);
		$_POST['width'] = preg_replace("/[^0-9]/","",$_POST['capacity']['width']);
		$_POST['height'] = preg_replace("/[^0-9]/","",$_POST['capacity']['depth']);
		if( $_POST['date'] == '日付を選択'){$_POST['date'] = '';}
		$_POST['shipment_date'] = str_replace($jp,"/",$_POST['date']);
		$_POST['specific'] = $_POST['spItems'];
	}

	$error = 0;
	$json_array=array(
		'error_message'=>array(

		)
	);

	if($_POST['from'] == $_POST['to']){
		$error++;
		array_push($json_array['error_message'],['from'=>"出発地と到着地が同じです"]);
	}

	if($_POST['date'] == ''){
		$error++;
		array_push($json_array['error_message'],['date'=>"形式が正しくありません"]);
	}


	if(intval($_POST['weight']) < 1 || strlen((string) $_POST['weight']) > 4) {
		$error++;
		array_push($json_array['error_message'],['weight'=>"整数4桁以内で入力してください"]);

	}

	if(intval($_POST['length']) < 1 || strlen((string) $_POST['length']) > 4) {
		$error++;
		array_push($json_array['error_message'],['length'=>"整数4桁以内で入力してください"]);
	}

	if(intval($_POST['width']) < 1 || strlen((string) $_POST['width']) > 4) {
		$error++;
		array_push($json_array['error_message'],['width'=>"整数4桁以内で入力してください"]);
	}

	if(intval($_POST['height']) < 1 || strlen((string) $_POST['height']) > 4) {
		$error++;
		array_push($json_array['error_message'],['height'=>"整数4桁以内で入力してください"]);

	}

	if(intval($_POST['quantity']) < 1 || !strlen((string) $_POST['quantity'])){
		$error++;
		array_push($json_array['error_message'],['quantity'=>"整数を入力してください"]);

	}

	$fixed_array['from'] = $_POST['from'];
	$fixed_array['to'] = $_POST['to'];
	$fixed_array['weight'] = $_POST['weight'];
	$fixed_array['length'] = $_POST['length'];
	$fixed_array['height'] = $_POST['height'];
	$fixed_array['width'] = $_POST['width'];
	$fixed_array['shipment_date'] = $_POST['shipment_date'];
	$fixed_array['specific'] = $_POST['specific'];
	$fixed_array['quantity'] = $_POST['quantity'];
	$_SESSION['POST'] = $fixed_array;

	$price = intval(calculate($fixed_array));


	if( $error ){
		header("Content-Type: text/javascript; charset=utf-8");
		echo json_encode($json_array,JSON_UNESCAPED_UNICODE);
		exit;
	}

}

$json_array = array(
	'total_price' => $price,
);

header("Content-Type: text/javascript; charset=utf-8");
echo json_encode($json_array);



?>