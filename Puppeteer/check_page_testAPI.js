//Puppeteerパッケージ読み込み
const puppeteer=require('puppeteer');

//Basic認証用
const USERNAME='demo';
const PASSWORD = 'AimD4TW2y3394JwI';

const URL_API='http://www.testweb.acx.ana-g.com/api/customer.html';
const URL_STG='http://www.testweb.acx.ana-g.com/ja/dom/customer.html';

//URL選択
//API：0
//stg：1
const FLAG=0;


async function test() {

    //ヘッドレスモード
    const browser=await puppeteer.launch();

    //ブラウザ表示モード
    // const browser = await puppeteer.launch({headless: false});


    const page = await browser.newPage();

    //Basic認証入力
    await page.setExtraHTTPHeaders({
        Authorization: `Basic ${new Buffer(`${USERNAME}:${PASSWORD}`).toString('base64')}`
    });

    //URLアクセス
    if(FLAG===0){
        await page.goto(URL_API, {
            waitUntil: "domcontentloaded"
        });
    } else if(FLAG===1) {
        await page.goto(URL_STG, {
            waitUntil: "domcontentloaded"
        });
    }

    //出発地と到着地を入れ替えて計算
    //上記以外の項目は固定
    for (let i = 1; i <52 ; i++) {

        //空のID飛ばす
        if(i===18||i===19||i===26){
            continue;
        }

        for (let j = i+1; j <52 ; j++) {

            //空のID飛ばす
            if(j===18||j===19||j===26){
                continue;
            }

            //出発地選択
            await page.select('select[name="from"]', i.toString());

            //到着地選択
            await page.waitFor(500);
            await page.select('select[name="to"]', j.toString());

            //カレンダークリック
            await page.waitFor(500);
            await page.click('.ui-datepicker-trigger');

            //日クリック
            await page.waitFor(500);
            if (FLAG===0) {
                await page.click('.ui-state-default');
            } else if (FLAG===1) {
                await page.click('.ui-datepicker-today');
            }


            //縦入力
            await page.waitFor(500);
            await page.type("input[name=\"capacity[height]\"]", "50");

            //横入力
            await page.waitFor(500);
            await page.type("input[name=\"capacity[width]\"]", "50");

            //奥行き入力
            await page.waitFor(500);
            await page.type("input[name=\"capacity[depth]\"]", "50");

            //重量入力
            await page.waitFor(500);
            await page.type("input[name=\"weight\"]", "50");

            //数量入力
            await page.waitFor(500);
            await page.type("input[name=\"quantity\"]", "50");

            //出発地値取得
            const departure = await page.$eval('#dep', item => {
                return item.textContent;
            });

            //到着地値取得
            const arrival = await page.$eval('#arr', item => {
                return item.textContent;
            });


            //検索ボタン
            if (FLAG===0) {
                await page.click('#submit');
            }else if(FLAG===1){
                await page.click('#test_search');
            }

            // await page.waitForNavigation({
            //     waitUntil: "domcontentloaded"
            // });
            await page.waitFor(500);


            let price;

            //運賃値取得
            if (FLAG===0) {
                price = await page.$eval('#test', item => {
                    return item.textContent;
                });

            }else if (FLAG===1) {
                price = await page.$eval('#test', item => {
                    return item.textContent.replace(/,/g, "");
                });
            }

            console.log(departure+","+arrival+","+price);


            //前のページに戻る
            await page.waitFor(500);
            await page.goBack();

            //リロード
            await page.waitFor(500);
            await page.reload();

            await page.waitFor(500);


        }
    }
    await page.close();
}


(async()=>{

    await test();

} )();
