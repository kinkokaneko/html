//必要なものを諸々読み込む
const fs = require("fs");
const path = require("path");
const csv = require("csv");
const puppeteer = require("puppeteer");

(async () => {
    //裏でChrome的なもの（Chromium）を起動して新しいタブを開く
    const browser = await puppeteer.launch();
    const page = await browser.newPage();

    //URLのリストを指定
    //別にただURLをバーって書いたテキストファイルでもいいんだけど、CSVだと別な情報も含めれて便利かなって思った
    //今回はタブ区切りだからTSVだけどね
    const URL_LIST  = path.resolve(__dirname, "urls.csv");
    const DELIMITER = '\t';

    //今回はBASIC認証かかってたのでこれで突破、かかってなければブロックごとコメントアウトしちゃおう
    //URLごとに異なるBASIC認証がかかっているなら、せっかくCSVにしたし、そっちに書いてもよい
    const USERNAME  = "BASIC認証のユーザー名";
    const PASSWORD  = "BASIC認証のパスワード";
    await page.setExtraHTTPHeaders({
        Authorization: `Basic ${Buffer.from(`${USERNAME}:${PASSWORD}`, "utf-8").toString("base64")}`
    });

    //async/awaitが見慣れない？
    //積極的に使っていかないとコールバック地獄になるよ
    await new Promise((resolve, reject) => {
        //CSVのパーサーを用意、1行目は列見出し扱いにする
        const parser = csv.parse({ delimiter: DELIMITER, columns: true });
        let readable = false;

        parser.on("readable", async () => {
            //なんか知らんけど readable が2回発生することがあってハマったので、2回目以降は実行しないことにしておく
            if (readable) return;
            readable = true;

            let record;
            while ((record = parser.read())) {
                //ここからCSV1行単位で処理できるよ
                const url = record["URL"];
                if (!url || !url.length) continue;

                try {
                    //アドレスバーにURLを入れてEnter、DOM構築が終わるまで待つ
                    //jQueryで言うといわゆる $(function(){ }); やね
                    await page.goto(url, { waitUntil: "domcontentloaded" });

                    //page.$eval を使えばいいんだけど、何度も書くの面倒なので page.evaluate を使ってしまう
                    const data = await page.evaluate(() => {
                        //ここのブロックは指定したページのコンテキストで実行されるよ
                        //ページ内で jQuery が読み込まれていれば 普通に jQuery が使える、良かったですね
                        //今回はこのブログで試すから jQuery と書いているけど、もちろんページ内で $ が使えるなら $ でもOK
                        const heading = jQuery("article .entry-title").text().trim();
                        const firstText = jQuery(".entry-content > :first-child").text().trim();
                        return { heading, firstText, url:location.href };
                    });

                    //ブラウザでもよく使う console.log を使えば標準出力、つまり、黒い画面上に結果を表示できる
                    //ファイルに出力してもいいのだけど、標準出力に出しちゃえばリダイレクトとかパイプが使えるしね
                    //フォーマットは適宜調整してね
                    //...はスプレッド構文で、配列を展開できるよ
                    console.log([...Object.values(record), ...Object.values(data)].join('\t'));
                } catch (e) {
                    //ちゃんとエラーキャッチしておこう
                    //ちなみに console.warn も黒い画面に出るけど、これはエラー出力だから普通のリダイレクトとかパイプでは渡されないよ
                    console.warn(`Error: ${e.message} ${url}`);
                }
            }

            //全件終わったら完了に
            resolve();
        });

        //CSVパースでエラーが出たら失敗扱いに
        parser.on("error", e => {
            console.error(e.message);
            reject();
        });

        //ファイルをダーッと流し込む
        fs.createReadStream(URL_LIST).pipe(parser);
    });

    //全部終わったらちゃんとChrome的なもの（Chromium）を閉じましょう
    await browser.close();
})();