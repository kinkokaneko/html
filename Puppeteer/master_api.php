<?php
// AREA
  $area_master = json_decode(file_get_contents('../ja/dom/air_info/cargo/assets/data/area.json'), True);
  $areas = $area_master['area'];

  $areas_often = $area_master['often'];


//BASIC
$basic_master = json_decode(file_get_contents('../ja/dom/air_info/cargo/assets/data/basic.json'), True);
$rules = $basic_master['rule'];

//Sunday discount
$sunday_master = json_decode(file_get_contents('../ja/dom/air_info/cargo/assets/data/sunday.json'), True);
$discounts = $sunday_master['rule'];

//Holidays discount
$holiday_master = json_decode(file_get_contents('../ja/dom/air_info/cargo/assets/data/holiday.json'), True);
$holidays = $holiday_master['holidays'];

//Specific
$specific_master = json_decode(file_get_contents('../ja/dom/air_info/cargo/assets/data/specific.json'), True);
$specific_rules = $specific_master['rule'];


//Priorities
$priorities_master = json_decode(file_get_contents('../ja/dom/air_info/cargo/assets/data/priority.json'), True);
$priorities = $priorities_master['rule'];

//Specific Items list
$specific_list = array(
    '',
    '生鮮魚介類',
    '果物・野菜',
    '畜産品',
    '新聞',
    '花弁園芸植物',
    '',
    '',
    '',
    '',
    '',
    '白金・金・銀・通貨・有価証券・宝石・美術品等',
    '荷送人から貴重品取扱いとして申告のあったもの',
    'その他国内貨物運送約款に定めたもの',
    '生きた動物（生きた魚を除く）',
    '遺体・遺骨',
    '危険物（国内貨物運送約款に定めたもの）',
    '運航イレギュラー時等の優先搭載輸送'
  );
