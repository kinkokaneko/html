'use strict'
{
    // document.body.textContent = "hello";
    // document.title = "changed!";

    // document.getElementById('target').textContent = 'changed';

    // const h1 = document.querySelector('h1')
    // console.log(h1.title)

    // h1.style.color = 'red'
    // h1.style.backgroundColor = 'blue'

    // const div = document.querySelector('div')

    // div.className = 'box border-pink';

    const h1 = document.createElement('h1');
    h1.textContent = 'title';
    document.body.appendChild(h1);

    //イベントリスナー
    const button = document.querySelector('button');

    button.addEventListener('dblclick', () => {
        console.log('clicked');
    });

    const div = document.querySelector('div');
    document.addEventListener('mousemove', e => {
        div.textContent = `${e.clientX}:${e.clientY}`;
    });


    const a = document.querySelector('a');
    const span = document.querySelector('span');

    a.addEventListener('click', e => {
        e.preventDefault();
        a.classList.add('hidden');
        span.classList.remove('hidden');

    });






}
